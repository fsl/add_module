#!/usr/bin/env python
#
# fsl_generate_module_manifest.py - Generate a template manifest.json file for
#                                   use by fsl_add_module.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""The ``fsl_generate_module_manifest`` script is intended for use by FSL
dataset maintainers. It can be used to generate a JSON manifest file which
describes a set of archive/plugin files that are to be made available for
download.
"""


import sys
import argparse
import contextlib as ctxlib
import os.path as op

from typing import List

import fsl.add_module.manifest as plgman
import fsl.add_module.routines as routines


def parseArgs(argv : List[str]) -> argparse.Namespace:
    """Parse command-line arguments. The user is expected to provide a set of
    input archive/plugin files and a destination file to save the JSON
    manifest. An existing manifest file/URL can optionally be provided -
    metadata will be copied over from this file for archive files with
    matching names.
    """

    parser = argparse.ArgumentParser(
        'fsl_generate_module_manifest',
        usage='fsl_generate_module_manifest [options] infile [infile ...]',
        description='Generate a manifest.json file for use by fsl_add_module')

    parser.add_argument('-m', '--manifest',
                        help='Previous version of manifest to copy metadata '
                             'from. The name, category, url, description, and '
                             'destination fields are all copied across for '
                             'archive files with matching names.')
    parser.add_argument('-o', '--outfile',
                        help='Location to save new manifest (default: stdout)')
    parser.add_argument('infile', nargs='+',
                        help='Archive file to include in new manifest.')

    return parser.parse_args(argv)


def main(argv=None):
    """Main routine. Generates a JSON manifest describing a set of files.
    """
    if argv is None:
        argv = sys.argv[1:]

    args = parseArgs(argv)

    if args.manifest is not None:
        oldmanifest = plgman.downloadManifest(args.manifest)
        oldmanifest = {p.rawFileName : p for p in oldmanifest.values()}
    else:
        oldmanifest = plgman.Manifest()

    newmanifest = plgman.Manifest()

    for infile in args.infile:
        basename = op.basename(infile)

        if basename in oldmanifest:
            plugin = oldmanifest[basename]
        else:
            plugin = plgman.Plugin(url='', name=basename)

        if plugin.origDestination is not None:
            plugin.destination = plugin.origDestination

        plugin.size              = op.getsize(infile)
        plugin.checksum          = routines.calcChecksum(infile)
        newmanifest[plugin.name] = plugin

    newmanifest = plgman.genManifest(newmanifest)

    if args.outfile is None:
        print(newmanifest)
    else:
        with open(args.outfile, 'wt', encoding='utf-8') as f:
            f.write(newmanifest)


if __name__ == '__main__':
    main()
