#!/usr/bin/env python
#
# fsl_add_module.py - FSL plugin/module downloader.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""The ``fsl_add_module`` script is used for downloading and installing FSL
"modules"/"plugins" - archive files (e.g. ``.tar.gz`` ``.zip``, etc.).

The primary use of this script is for downloading the FSL course data.

Normal execution of this script is as follows:

 1. The user invokes the script, specifying the plugin they would like to
    download.
 2. An online "manifest" file is downloaded - this file contains a list of
    all available plugins.
 3. The URL for the requested plugin is looked up in the manifest.
 4. The plugin file is downloaded and installed.

All information about a particular plugin is contained in the manifest file,
including:

 - the download URL
 - the default installation directory
 - a checksum to verify that the correct file was downloaded

However, the ``fsl_add_module`` script can be used without a manifest file,
by specifying a URL or file path to an archive file. The user will be prompted
for any required information, or they may specify it on the command-line.


Plugin manifest file
--------------------

The plugin manifest file is a JSON file which contains information about all
plugins that are available. An official manifest file is hosted on the FSL
website, but an alternative manifest file may be specified when this script is
invoked. See the :mod:`.manifest` module for more details on the format of a
plugin manifest file.


Usage examples
--------------

::

    fsl_add_module
    fsl_add_module -m <manifest_url>
    fsl_add_module -c <category>
    fsl_add_module -m <manifest_url> -c <category>

When called in one of the above forms, ``fsl_add_module`` will:

 1. Download a manifest file from the default :data:`DEFAULT_MANIFEST_URL`, or
    ``<manifest_url>`` if specified

 2. Display a list of all available plugins to the user (restricting to those
    which match ``<category>``, if specified) prompting them to select which
    plugins they would like to install.

 3. Ask the user to confirm the destination directory for each requested
    plugin.

 4. Download and install each plugin.


::

    fsl_add_module                   <name_or_url> [<name_or_url> ...]
    fsl_add_module -m <manifest_url> <name_or_url> [<name_or_url> ...]

When called in one of the above forms, ``fsl_add_module`` will:

 1. Download a manifest file from the default :data:`MANIFEST_URL`, or
    ``<manifest_url>`` if specified

 3. Ask the user to confirm the destination directory for each requested
    plugin.

 4. Download and install each plugin.


::

    fsl_add_module -l
    fsl_add_module -l -m <manifest_url>

When called in one of the above forms, ``fsl_add_module`` will:

 1. Download a manifest file from the default :data:`MANIFEST_URL`, or
    ``<manifest_url>`` if specified

 2. Print a list of all available plugins and exit.
"""


import os.path        as op
import                   os
import                   sys
import                   argparse

from typing import List, Tuple

import fsl.add_module.ui        as     ui
import fsl.add_module.routines  as     routines
import fsl.add_module.manifest  as     plgman
from   fsl.add_module           import __version__
from   fsl.add_module.messages  import (info,
                                        important,
                                        warning,
                                        error,
                                        EMPHASIS,
                                        UNDERLINE)


OFFICIAL_MANIFEST_URLS = {
    'fslcourse'  : 'http://fsl.fmrib.ox.ac.uk/fslcourse/downloads/manifest.json',
    'gradcourse' : 'https://fsl.fmrib.ox.ac.uk/fslcourse/graduate/downloads/manifest.json'
}
"""Locations of the official FSL plugin manifest files, downloaded if an
alternate manifest file is not specified. These 'built-in' manifest URLs
can be referred to by their aliases.
"""


DEFAULT_MANIFEST = 'fslcourse'
"""Default manifest to use, if one is not specified."""


DEFAULT_CATEGORY = 'fsl_course_data'
"""Default value for the ``--category`` command-line option, when the
user does not specify an alternative ``--manifest`` or ``--category``.
"""


ARCHIVE_DIR = op.expanduser(op.join('~', '.fsl_add_module'))
"""Location in which downloaded plugin files are cached. """


def parseArgs(argv : List[str]) -> argparse.Namespace:
    """Parses command-line arguments and returns an ``argparse.Namespace``
    object.
    """

    helps = {
        'version'     : 'Print version and exit.',
        'verbose'     : 'Output more information.',
        'list'        : 'Print all available modules and exit. All other '
                        'options, apart from --manifest and --verbose, are '
                        'ignored.',
        'module'      : 'Name or URL of FSL module to download.',
        'manifest'    : 'Name of official manifest file to download, or '
                        'alternate URL to module manifest file (default: '
                        f'{DEFAULT_MANIFEST})',
        'archiveDir'  : 'Directory to cache downloaded files in.',
        'category'    : 'Only display available modules from the specified '
                        'category. Defaults to "fsl_course_data", unless '
                        'a custom --manifest is specified. Pass "all" to '
                        'display modules from all categories.',
        'destination' : 'Destination directory to install module files. If '
                        'used once, applies to all modules. Otherwise, must '
                        'be specified for each module that is specified on '
                        'the command-line. If not provided, you will be '
                        'prompted to select the destination directory for '
                        'each module.',
        'force'       : 'If used, you will not be asked any questions, '
                        'all default settings will be applied, and all '
                        'available modules will be downloaded.'
    }

    parser = argparse.ArgumentParser(
        'fsl_add_module',
        usage='fsl_add_module [options] [modules]',
        description='Download and install FSL modules')
    parser.add_argument(
        '-V', '--version', action='version', help=helps['version'],
        version='%(prog)s {}'.format(__version__))
    parser.add_argument(
        '-v', '--verbose', action='store_true', help=helps['verbose'])
    parser.add_argument(
        '-l', '--list', action='store_true', dest='listPlugins',
        help=helps['list'])
    parser.add_argument('-m', '--manifest', help=helps['manifest'])
    parser.add_argument('-c', '--category', help=helps['category'])
    parser.add_argument(
        '-a', '--archiveDir', default=ARCHIVE_DIR, help=helps['archiveDir'])
    parser.add_argument(
        '-d', '--destination', action='append', help=helps['destination'])
    parser.add_argument(
        '-f', '--force', action='store_true', help=helps['force'])
    parser.add_argument('module', nargs='*', help=helps['module'])

    args = parser.parse_args(argv)

    # If neither a custom manifest nor category are
    # provided, we default to only showing the FSL
    # course data. If we are using a custom manifest,
    # we don't want to set a default category.
    if args.manifest is None:
        args.manifest = DEFAULT_MANIFEST
    if args.manifest == DEFAULT_MANIFEST and args.category is None:
        args.category = DEFAULT_CATEGORY
    if args.category == 'all':
        args.category = None

    # Built-in manifest referred to by alias, e.g. "fslcourse"?
    if not (args.manifest is None            or
            args.manifest.startswith('http') or
            op.exists(args.manifest)):
        try:
            args.manifest = OFFICIAL_MANIFEST_URLS[args.manifest]
        except KeyError:
            warning(f'Unknown manifest: {args.manifest}. Known manifests: ' +
                    ", ".join(OFFICIAL_MANIFEST_URLS.keys()) + ' (or give a '
                    'URL to an alternative manifest file)')

    if args.archiveDir:
        args.archiveDir = op.abspath(op.expanduser(args.archiveDir))

    if args.destination is not None:
        if len(args.destination) not in (1, len(args.module)):
            parser.error('The --destination option must either be specified '
                         'exactly once, or once for every requested module.')

    # make all destination paths absolute,
    # and support tilde/envvar expansion
    if args.destination is not None:
        args.destination = [routines.expand(d) for d in args.destination]

    return args


def loadManifest(args : argparse.Namespace) -> Tuple[plgman.Manifest,
                                                     List[str]]:
    """Loads/downloads the plugin manifest, and merges in any plugin files
    that were specified on the command-line.

    :arg args: ``argparse.Namespace`` containing parsed CLI arguments.
    :returns:  A tuple containing:
                - the loaded plugin :class:`.Manifest`.
                - A list of the names of any plugins which were explicitly
                  requested on the command-line, if any.
    """

    manifest = ui.downloadPluginManifest(args.manifest)
    plugins  = list(args.module)

    # assume that any plugins that were specified
    # on the command line, and which are not in
    # the manifest, are URLs/file paths - add
    # entries to the manifest for them
    for i, p in enumerate(plugins):
        if p not in manifest:
            plugins[i] = manifest.addPlugin(p)

    if (len(plugins) == 0) and (len(manifest) == 0):
        raise RuntimeError('No modules listed in manifest '
                           'or requested on command-line!')

    # if user specified destination(s) on the
    # command-line, it overrides any plugin
    # defaults.
    if args.destination is not None:
        dests = list(args.destination)

        # they either provided one destination
        # for each requested plugin
        if len(dests) == len(plugins):
            for p, d in zip(plugins, dests):
                manifest[p].destination     = d
                manifest[p].origDestination = d

        # or they provided one destination
        # to be applied to all plugins (but
        # if they requested one plugin, the
        # above rule takes precedence)
        elif len(dests) == 1:
            for p in manifest.keys():
                manifest[p].destination     = dests[0]
                manifest[p].origDestination = dests[0]

    return manifest, plugins


def selectPlugins(args     : argparse.Namespace,
                  manifest : plgman.Manifest,
                  plugins  : List[str]) -> List[plgman.Plugin]:
    """Ensures that all plugins requested on the command line have an entry
    in the plugin manifest, and/or prompts the user to select which plugins
    they would like to install. Also asks the user to confirm the installation
    destination directory for each requested plugin.

    :arg args:     ``argparse.Namespace`` object containing parsed
                   command-line arguments.
    :arg manifest: :class:`.Manifest` object describing all available plugins.
    :arg plugins:  List of names of plugins that were explicitly specified on
                   the command-line.
    :returns:      A list of :class:`.Plugin` objects representing the plugins
                   that are to be installed.
    """

    # if no plugins specified on command-line,
    # present the user with a list of available
    # plugins, and ask them which ones they
    # want to install
    if len(plugins) == 0:
        if args.force: plugins = manifest.getCategory(args.category)
        else:          plugins = ui.selectPlugins(manifest, args.category)
    else:
        plugins = [manifest[p] for p in plugins]
        info('Installing the following modules:', EMPHASIS)
        info('')
        for p in plugins:
            ui.printPlugin(p)
        info('')

    # warn the user if any plugins have a default
    # destination that is based on an unset
    # environment variable. If --force is active,
    # this causes the installation to be aborted.
    abort = False
    for plugin in plugins:
        envvars = routines.envvars(plugin.origDestination)
        for _, name in envvars:
            if name not in os.environ:
                abort = args.force
                warning(f'WARNING: Default destination for the '
                        f'[{plugin.name}] module is based on an '
                        f'unset environment variable '
                        f'({plugin.origDestination})!',
                        EMPHASIS, wrap=True)
    if abort:
        raise RuntimeError('Aborting installation - cannot use --force '
                           'when an installation destination depends on '
                           'an unset environment variable.')

    # if the user did not specify install
    # destinations, ask them to confirm
    # the destination directory for each
    # plugin (unless --force is active,
    # in which case defaults are used)
    if all((len(plugins) > 0,
            args.destination is None,
            not args.force)):
        ui.confirmDestination(plugins)

    return plugins


def download(args : argparse.Namespace, plugins : List[plgman.Plugin]):
    """Downloads and verifies all of the specified ``plugins``,

    :arg args:    ``argparse.Namespace`` object.
    :arg plugins: List of :class:`.Plugin` objects describing the plugins
                  to download.
    """

    important(f'Downloading {len(plugins)} module files...', EMPHASIS)
    info('If you would like to stop a download, you can press "CTRL+C" '
         'to exit. You can then re-run fsl_add_module at a later point '
         'in time to resume the download.', indent=2, wrap=True)

    for plugin in plugins:

        skipped = False

        # if plugin was specified as a local
        # file, we skip the download
        if plugin.archiveFile is None:
            try:
                archiveFile, skipped = ui.downloadPlugin(
                    plugin, args.archiveDir)
                plugin.archiveFile   = archiveFile

            except Exception as e:
                error(str(e))
                continue

        if not skipped and plugin.checksum is not None:
            ui.verifyDownload(plugin)


def install(args : argparse.Namespace, plugins : List[plgman.Plugin]):
    """Installs all of the downloaded plugins.

    :arg args:    ``argparse.Namespace`` object containing command-line
                  arguments.
    :arg plugins: List of :class:`.Plugin` objects describing the plugins to
                  be installed.
    """
    for plugin in plugins:
        ui.installPlugin(plugin)


def main(argv=None):
    """Entry point for the ``fsl_add_module`` script. """

    if argv is None:
        argv = sys.argv[1:]

    args = parseArgs(argv)

    important(f'fsl_add_module {__version__}. Press CTRL+C '
              'at any point to exit.', UNDERLINE, EMPHASIS)

    try:

        manifest, plugins = loadManifest(args)

        if args.listPlugins:
            ui.listPlugins(manifest, args.verbose)

        else:
            plugins = selectPlugins(args, manifest, plugins)
            if len(plugins) > 0:
                ui.createArchiveDir(args.archiveDir)
                download(           args, plugins)
                install(            args, plugins)

    except Exception as e:
        error(str(e), EMPHASIS, wrap=True)
        sys.exit(1)


if __name__ == '__main__':
    main()
