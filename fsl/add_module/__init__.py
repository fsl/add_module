#!/usr/bin/env python
#
# __init__.py - The fsl.add_module package.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>

"""The :mod:`fsl.add_module` package contains routines used by the
:mod:`fsl.scripts.fsl_add_module` script.
"""


__version__ = '0.5.1'
"""``fsl_add_module`` version number."""
