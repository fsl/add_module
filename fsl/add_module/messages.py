#!/usr/bin/env python
#
# messages.py - Print things to the terminal in colour.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module contains some simple functions which allow things to be printed
to standard output, with colour, emphasis, line wrapping, and indentation.
"""


import enum
import shutil
import textwrap

from typing import Union


class MSGTYPES(enum.Enum):
    """List of modifiers which can be used to change how a message is printed.
    """
    INFO      = enum.auto()
    IMPORTANT = enum.auto()
    QUESTION  = enum.auto()
    PROMPT    = enum.auto()
    WARNING   = enum.auto()
    ERROR     = enum.auto()
    EMPHASIS  = enum.auto()
    UNDERLINE = enum.auto()
    RESET     = enum.auto()


INFO      = MSGTYPES.INFO
IMPORTANT = MSGTYPES.IMPORTANT
QUESTION  = MSGTYPES.QUESTION
PROMPT    = MSGTYPES.PROMPT
WARNING   = MSGTYPES.WARNING
ERROR     = MSGTYPES.ERROR
EMPHASIS  = MSGTYPES.EMPHASIS
UNDERLINE = MSGTYPES.UNDERLINE
RESET     = MSGTYPES.RESET


ANSICODES = {
    MSGTYPES.INFO      : '\033[37m',
    MSGTYPES.IMPORTANT : '\033[92m',
    MSGTYPES.QUESTION  : '\033[36m\033[4m',
    MSGTYPES.PROMPT    : '\033[36m\033[1m',
    MSGTYPES.WARNING   : '\033[93m',
    MSGTYPES.ERROR     : '\033[91m',
    MSGTYPES.EMPHASIS  : '\033[1m',
    MSGTYPES.UNDERLINE : '\033[4m',
    MSGTYPES.RESET     : '\033[0m',
}
"""ANSI code for each modifier in :attr:`MSGTYPES`."""


def printmsg(msg : str,
             *msgtypes,
             wrap   : Union[bool, int] = False,
             indent : int              = 0,
             **kwargs):
    """Prints ``msg`` according to the following rules:

      - Any ANSI codes in ``args`` are applied
      - If ``wrap`` is specified, ``msg`` is split across multiple lines,
        each having maximum length of ``wrap - indent`` characters. If
        ``wrap`` is ``True``, it is set to the current terminal width
        (via ``shutil.get_terminal_size()``).
      - Each line in ``msg`` is indented by ``indent`` space characters,

    All other keyword arguments are passed through to the ``print`` function.
    """

    if wrap is True:
        wrap = min((80, shutil.get_terminal_size()[0]))

    msgcodes = [ANSICODES[t] for t in msgtypes]
    msgcodes = ''.join(msgcodes)

    if bool(wrap): msg = textwrap.wrap(msg, wrap)
    else:          msg = msg.split('\n')

    pad = ' ' * indent
    msg = '\n'.join([f'{pad}{line}' for line in msg])

    print(f'{msgcodes}{msg}{ANSICODES[MSGTYPES.RESET]}', **kwargs)


def info(msg : str, *args, **kwargs):
    """Prints ``msg``, applying the ``MSGTYPES.INFO`` code. """
    printmsg(msg, MSGTYPES.INFO, *args, **kwargs)


def important(msg : str, *args, **kwargs):
    """Prints ``msg``, applying the ``MSGTYPES.IMPORTANT`` code. """
    printmsg(msg, MSGTYPES.IMPORTANT, *args, **kwargs)


def question(msg : str, *args, **kwargs):
    """Prints ``msg``, applying the ``MSGTYPES.QUESTION`` code. """
    printmsg(msg, MSGTYPES.QUESTION, *args, **kwargs)


def warning(msg : str, *args, **kwargs):
    """Prints ``msg``, applying the ``MSGTYPES.WARNING`` code. """
    printmsg(msg, MSGTYPES.WARNING, *args, **kwargs)


def error(msg : str, *args, **kwargs):
    """Prints ``msg``, applying the ``MSGTYPES.ERROR`` code. """
    printmsg(msg, MSGTYPES.ERROR, *args, **kwargs)


def prompt(msg       : str,
           *args,
           allowQuit : bool = False,
           strip     : bool = True,
           default   : str  = '',
           **kwargs) -> str:
    """Prompt the user to enter some text, and return that text,

    :arg allowQuit: If ``True``, and the user enters ``'q'`` or ``''quit'``,
                    :exc:`SystemExit` is raised.
    :arg default:   Default value to return if the user pushes enter without
                    typing anything else.
    :arg strip:     If ``True`` (the default), leading/trailing whitespace is
                    removed from the response.

    All other arguments are passed to the :func:`question` function.
    """
    printmsg(msg, MSGTYPES.PROMPT, *args, end=' ', **kwargs)

    response = input()

    if allowQuit and response.strip().lower() in ('q', 'quit'):
        raise SystemExit()

    if response == '':
        return default

    if strip:
        response = response.strip()

    return response
