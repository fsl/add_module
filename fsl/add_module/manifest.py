#!/usr/bin/env python
#
# manifest.py - Classes and functions for working with FSL plugin
#               manifest files.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module contains classes and functions for working with FSL plugin
manifest files. A plugin manifest file is a JSON file which contains a
list of FSL plugin definitions, e.g.::

    [
        {
            "name"        : "Unix Introduction",
            "category"    : "fsl_course_data",
            "url"         : "http://.../UnixIntro.zip",
            "checksum"    : "f8222...",
            "size"        : "15677939",
            "description" : "Data for the unix introduction",
            "version"     : "1.2.3",
            "destination" : "~/"
        },
        {
            "name"         : "ABCD123 Mouse atlas",
            "category"     : "fsl_atlases",
            "url"          : "http://localhost:8000/abcd123_mouse_atlas.tar.gz",
            "checksum"     : "658701ff9a49b06f0d0ee38bce36a1c370fdfd0751ab17c2fd7b7b561d3b92e0",
            "size"         : "123841532",
            "description"  : "ABCD123 FSL mouse atlas derived from hand segmentations of 3813 small chocolate mice.",
            "terms_of_use" : "https://chocolate-mouse.com/terms-of-use",
            "destination"  : "$FSLDIR/data/atlases/"
        }
    ]

Each plugin definition must contain the ``url`` field; all other fieids are
optional but recommended:

  - ``name``:         A unique name for the plugin. Defaults to the filename
                      component of the ``url``.
  - ``url``:          URL of the plugin archive file.
  - ``category``:     The plugin category, used to organise plugins.
  - ``checksum``:     A SHA256 checksum of the plugin archive file, used to
                      verify that it has been successfully downloaded.
  - ``size``:         Size of the file in bytes
  - ``description``:  An extended description of the plugin
  - ``version``:      A version identifier for the plugin (used solely for
                      descriptive purposes)
  - ``terms_of_use``: Terms of use for the plugin (displayed to the user)
  - ``destination``:  Default installation directory. May contain environment
                      variables and ``~``.
"""  # noqa: E501


import os.path        as op
import                   os
import                   json
import                   logging
import                   pathlib
import                   tempfile
import                   dataclasses
import urllib.parse   as urlparse
import urllib.request as urlrequest

from typing import Union, Optional, List

import fsl.add_module.routines as routines


log = logging.getLogger(__name__)


class ManifestInvalid(Exception):
    """Exception raised by the :func:`downloadManifest` function if a
    manifest file appears to be invalid.
    """


UNCATEGORISED = 'uncategorised'
"""Category label used for otherwise uncategorised plugins."""


@dataclasses.dataclass
class Plugin:
    """Simple data class which represents a single entry in a FSL plugin
    manifest file.
    """

    name : str
    """Name used to uniquely identify this plugin."""

    url : Union[str, pathlib.Path]
    """URL of the plugin archive file. """

    category : str = UNCATEGORISED
    """Plugin category - plugins may be grouped by category."""

    description : Optional[str] = None
    """Description of the plugin. """

    checksum : Optional[str] = None
    """SHA256 checksum of the plugin archive file, used to verify downloads.
    """

    size : Optional[int] = None
    """Size of the archive file in bytes. """

    version : Optional[str] = None
    """Version identifier for the plugin - displayed to the user, but not
    used for any other purpose.
    """

    terms_of_use : Optional[str] = None
    """Terms of use for the plugin - displayed to the user during installation.
    """

    destination : Union[None, str, pathlib.Path] = None
    """Installation/destination directory - where the plugin archive file
    will be extracted. May have a default value in the manifest, but may
    be overridden at user request. Any environment variables and the ``~``
    character will be replaced with the variable value/user home directory.
    """

    origDestination : Union[None, str, pathlib.Path] = None
    """Original value of ``destination``, before environment variable/tilde
    expansion.

    If the original destination contains any environment variables that
    are not set, the user is warned.
    """

    archiveFile : Union[None, str, pathlib.Path] = None
    """Path to downloaded archive file (manually assigned once the file has
    been downloaded).
    """

    checksumPassed : Optional[bool] = None
    """Flag indicating whether the checksum of the downloaded file matched
    the checksum in the manifest (manually assigned once the file has been
    downloaded).
    """


    @property
    def fileName(self):
        """Return a suitable name to use for the downloaded plugin file.
        The file name may be prefixed with the plugin file category.
        """
        fname = self.rawFileName
        if self.category not in (None, UNCATEGORISED):
            fname = f'{self.category}_{fname}'
        return routines.sanitiseFileName(fname)


    @property
    def rawFileName(self):
        """Return the file name of the plugin file as it appears in te URL.
        """
        return op.basename(urlparse.urlparse(self.url).path)


class Manifest(dict):
    """The ``Manifest``class simply a container for :class:`Plugin` instances.
    A ``Manifest`` is a dict containing ``{name : Plugin}`` mappings.

    The :meth:`addPlugin` method may be used to add a new :class:`Plugin` to
    the manifest
    """


    def addPlugin(self,
                  url          : Union[str, pathlib.Path],
                  name         : Optional[str] = None,
                  description  : Optional[str] = None,
                  category     : Optional[str] = None,
                  checksum     : Optional[str] = None,
                  size         : Optional[int] = None,
                  version      : Optional[str] = None,
                  terms_of_use : Optional[str] = None,
                  destination  : Optional[str] = None) -> str:
        """Add a new :class:`Plugin` to the manifest.

        :arg url:          URL of plugin archive file.
        :arg name:         Plugin name. Defaults to filename component of
                           ``url``.
        :arg description:  Extended description of plugin
        :arg category:     Plugin category
        :arg checksum:     SHA256 checksum of archive file.
        :arg size:         Size of archive file in bytes.
        :arg version:      Version identifier for the plugin.
        :arg terms_of_use: Terms of use for the plugin.
        :arg destination:  Default installation directory.
        :returns:          The registered plugin name (equivalent to ``name``,
                           if provided).
        """

        # support paths to local files
        if op.exists(url):
            url         = routines.expand(url)
            archiveFile = url
            url         = urlparse.urljoin(
                'file:', urlrequest.pathname2url(url))
        else:
            archiveFile = None

        # name defaults to filename in URL
        if name is None:
            name = op.basename(urlparse.urlparse(url).path)

        if category is None:
            category = UNCATEGORISED

        # install location defaults to $FSLDIR
        # if set or, failing that, to pwd
        if destination is None:
            destination = os.environ.get('FSLDIR', os.getcwd())

        # allow env vars and tilde in
        # JSON destination field
        origDestination = destination
        destination     = routines.expand(destination)

        if name in self:
            log.warning('Module with name %s [%s] already exists! Overwriting '
                        'with [%s]', name, self[name].url, url)

        self[name] = Plugin(
            name=name,
            url=url,
            category=category,
            description=description,
            checksum=checksum,
            size=size,
            version=version,
            terms_of_use=terms_of_use,
            destination=destination,
            origDestination=origDestination,
            archiveFile=archiveFile,
            checksumPassed=None)

        log.debug('Registered plugin" %s', self[name])

        return name


    @property
    def categories(self) -> List[str]:
        """Returns a list of all plugin categories in the manifest. """
        cats = [p.category for p in self.values()]
        return list(sorted(set(cats)))


    def getCategory(self, category : Optional[str]) -> List[Plugin]:
        """Returns a list of all available plugins in the specified
        ``category``. If ``category is None``, all plugins are returned.
        """
        def test(p):
            return category in (None, p.category)

        return list([p for p in self.values() if test(p)])


def downloadManifest(url : Union[str, pathlib.Path]) -> Manifest:
    """Downloads a FSL plugin manifest from the given ``url``.

    Raises a :exc:`ManifestInvalid` error if the manifest file appears to
    be invalid in any way.

    :arg url: URL to the manifest file
    :returns: A :class:`Manifest` object.
    """
    log.info('Downloading manifest file from %s...', url)

    # allow local paths in addition
    # to urls for the manifest file
    if op.exists(url):
        url = urlparse.urljoin(
            'file:', urlrequest.pathname2url(op.abspath(url)))

    manifest = Manifest()

    with tempfile.TemporaryDirectory() as d:
        dest = op.join(d, 'manifest.txt')
        routines.downloadFile(url, dest, progress=False)
        rawmanifest = open(dest, 'rt', encoding='utf-8').read()

    try:
        rawmanifest = json.loads(rawmanifest)
    except json.decoder.JSONDecodeError as e:
        raise ManifestInvalid(f'The manifest file {url} '
                              'is not a valid JSON file') from e

    for plugin in rawmanifest:

        # plugins must at least contain a URL
        try:
            try:              size = int(plugin.get('size'))
            except Exception: size = None
            manifest.addPlugin(
                url=plugin['url'],
                name=plugin.get('name'),
                description=plugin.get('description'),
                category=plugin.get('category'),
                checksum=plugin.get('checksum'),
                size=size,
                version=plugin.get('version'),
                terms_of_use=plugin.get('terms_of_use'),
                destination=plugin.get('destination'))
        except KeyError as e:
            raise ManifestInvalid(f'The manifest file {url} '
                                  'contains an invaluid module '
                                  'definition: {plugin}') from e

    return manifest


def genManifest(manifest : Manifest) -> str:
    """Converts the given manifest to JSON. """

    fields  = ['name', 'url', 'category', 'checksum', 'size', 'description',
               'version', 'destination', 'terms_of_use']
    plugins = list(manifest.values())
    plugins = [dataclasses.asdict(p)                           for p in plugins]
    plugins = [{f : p[f] for f in fields}                      for p in plugins]
    plugins = [{k : v for k, v in p.items() if v is not None } for p in plugins]
    return json.dumps(plugins, indent=4)
