#!/usr/bin/env python
#
# admin.py - Routines used by fsl_add_module which need elevated
#            privileges.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module provides the :func:`canWrite` and :func:`runAsAdmin` functions,
for determining whether administrative privileges are required to write to
a destination, and for running a function with administrative privileges.


The :func:`runAsAdmin` function executes this module via ``sudo``, using the
current ``sys.interpreter``. The :func:`main` function of this module expects
to be passed the name of a function defined in the :mod:`.routines` module,
along with any arguments to be passed.
"""


import subprocess as sp
import os.path    as op
import               os
import               sys
import               shlex
import               pathlib

from typing import Union, Optional, List, Callable

from fsl.add_module.messages import error


def canWrite(dirname : Union[str, pathlib.Path]) -> bool:
    """Returns ``True`` if it appears that the user can write to the given
    destination (assumed to be a path to a directory), or ``False`` if the user
    does not appear to have permission to write to the specified directory (and
    would therefore require administrative privileges to do so).
    """
    return os.access(dirname,
                     os.W_OK | os.X_OK,
                     effective_ids=os.supports_effective_ids)


def runAsAdmin(routine : str, *argv : str):
    """Runs the given routine with administrative privileges. The user will
    be prompted to enter their password if necessary.

    :arg routine: Name of a function defined in the
                  :mod:`fsl.add_module.routines` module. Only functions which
                  accept strings for all of their arguments are supported.
    :arg argv:    Arguments to pass to ``routine``.
    """

    command = op.abspath(sys.executable)
    thismod = op.abspath(__file__)
    argv    = ['sudo', command, thismod] + [routine] + list(argv)
    argv    = [shlex.quote(a) for a in argv]
    result  = sp.run(' '.join(argv), shell=True, check=False)

    if result.returncode != 0:
        error(f'Admin call failed: {argv}', wrap=False)
        raise RuntimeError(' '.join(argv))


def lookup(func : str) -> Callable:
    """Looks up and returns a reference to a function which is named ``func``,
    which is defined in the :mod:`fsl.add_module.routines` module.
    """

    import fsl.add_module.routines as routines
    return getattr(routines, func)


def main(argv : Optional[List[str]] = None):
    """Entry point for running a function with administrative privileges.

    The first argument is expected to be the name of a function defined in the
    :mod:`.routines` module. All remaining arguments are passed to that
    function.

    :arg argv: Command-line arguments. If not provided, taken from
               ``sys.argv[1:]``.
    """
    if argv is None:
        argv = sys.argv[1:]

    func = argv[0]
    args = argv[1:]
    func = lookup(func)

    func(*args)


if __name__ == '__main__':
    main()
