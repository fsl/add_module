#!/usr/bin/env python
#
# ui.py - User interface for fsl_add_module.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module contains functions which implement the user-interaction aspects
of the ``fsl_add_module`` program.
"""

import            os
import os.path as op
import            pathlib

from typing import Tuple, List, Union

from fsl.add_module.routines import (expand,
                                     calcChecksum,
                                     downloadFile,
                                     CorruptArchive,
                                     extractArchive)
from fsl.add_module.manifest import (Plugin,
                                     Manifest,
                                     downloadManifest)
from fsl.add_module.messages import (info,
                                     important,
                                     question,
                                     warning,
                                     error,
                                     prompt,
                                     EMPHASIS,
                                     UNDERLINE)


def downloadPluginManifest(url : Union[str, pathlib.Path]) -> Manifest:
    """Downloads and loads a plugin manifest file from ``url``.

    This is just a wrapper around :func:`.routines.downloadManifest` which
    prints some messages. If the file cannot be downloaded/loaded, an
    empty :class:`.Manifest` is created and returned.

    :arg url:  URL of manifest file
    :returns: :class:`.Manifest` object
    """
    try:
        info(f'Downloading plugin manifest from {url}...')
        manifest = downloadManifest(url)
    except Exception as e:
        warning(f'Unable to download plugin manifest from {url}: {str(e)}')
        manifest = Manifest()
    return manifest


def printPlugin(plugin  : Plugin,
                index   : int  = None,
                verbose : bool = False):
    """Prints an overview of ``plugin``. Used by :func:`listPlugins` and
    :func:`selectPlugins`.

    :arg plugin:  The :class:`.Plugin` to print.
    :arg index:   Plugin index number (optional).
    :arg verbose: If ``True``, more information is printed.
    """

    header = plugin.name

    if index is not None:
        header = f'{index:2d} {header}'
    if plugin.size is not None:
        mbytes = plugin.size / 1048576
        header = f'{header} [{mbytes:0.2f} MB]'

    info(header, EMPHASIS, indent=2)

    if plugin.version is not None:
        info(f'[version {plugin.version}]', indent=4)

    if plugin.description is not None:
        info(plugin.description, indent=4, wrap=True)

    if plugin.terms_of_use is not None:
        info('Installation of this plugin is subject '
             'to the following terms of use:',
             indent=4)
        info(plugin.terms_of_use, indent=6, wrap=True)

    if verbose:
        for item in ('url', 'destination', 'checksum'):
            value = getattr(plugin, item)
            item  = f'{item}:'
            if value in (None, ''):
                value = 'N/A'
            info(f'{item:12s} {value}', indent=4)


def listPlugins(manifest : Manifest, verbose : bool = False):
    """Prints a list of all available plugins.

    :arg manifest: :class:`.Manifest` object describing all available plugins.
    :arg verbose:  If true, more information is printed.
    """
    important('Modules available for download:', EMPHASIS)

    categories = manifest.categories

    for category in categories:
        info(f'Category: {category}', UNDERLINE)
        plugins = manifest.getCategory(category)

        for plugin in plugins:
            printPlugin(plugin, verbose=verbose)


def createArchiveDir(archiveDir : Union[str, pathlib.Path]):
    """Creates the directory used to store/cache downloaded plugin archive
    files.

    :arg archiveDir: Path to archive directory
    """
    info(f'Downloaded module files will be cached in {archiveDir}')
    try:
        os.makedirs(archiveDir, exist_ok=True)
    except Exception as e:
        raise RuntimeError('Unable to create archive directory '
                           f'{archiveDir}!') from e


def _selectOnePlugin(plugin : Plugin) -> List[Plugin]:
    """Sub-function of :func:`selectPlugins` called when there is only one
    plugin available to install. Asks the user if they would like to install
    the plugin.

    :arg plugin: The available plugin
    :returns:    Either ``[plugin]``, or ``[]``, depending on whether
                 the user selected the plugin.
    """

    question(f'Do you want to download the [{plugin.name}] module?', EMPHASIS)
    info('Press enter or type Y to confirm the installation. Any other '
         'response will cancel the download.', indent=2, wrap=True)

    response = prompt(f'Install [{plugin.name}]? (Y/n): ')
    if response.lower() in ('', 'y', 'yes'):
        return [plugin]
    else:
        return []


def _selectMultiplePlugins(plugins : List[Plugin]) -> List[Plugin]:
    """Sub-function of :func:`selectPlugins` called when there are multiple
    plugins available to install. Prompts the user to select which plugins to
    install.

    :arg plugins: List of available plugins.
    :returns:     List of plugins selected by the user.
    """

    while True:

        info('Type "all" to download all of the modules that are listed. '
             'Alternately, enter the numbers of each module you would like '
             'to download, separated by spaces. For example, if you would '
             f'like to download the [{plugins[0].name}] and '
             f'[{plugins[1].name}] modules, enter "1 2".', indent=2, wrap=True)

        response = prompt('Enter module(s) to download: ')

        if response.lower() == 'all':
            break

        try:
            response = [int(i) for i in response.split()]
        except Exception:
            error(f'Specified module(s) {response} not understood')
            continue
        if len(response) == 0:
            warning('No modules specified! Type "all" to '
                    'download all modules.', wrap=True)
            continue
        if any([i <= 0 or i > len(plugins) for i in response]):
            error(f'One of the requested modules [{response}] does not exist')
            continue

        plugins = [plugins[i - 1] for i in response]
        break
    return plugins


def selectPlugins(manifest : Manifest, category : str = None) -> List[Plugin]:
    """Prompts the user to select which plugins they would like to download
    and install.

    :arg manifest: Dict of ``{name : Plugin}`` mappings of all available
                   plugin files.

    :arg category: If provided, only plugins in the specified category will be
                   given as options.

    :returns:      A list of :class:`.Plugin` objects representing the plugins
                   that the user selected.
    """

    plugins = list(manifest.values())

    if category is not None:
        plugins = [p for p in plugins if p.category == category]

    if len(plugins) == 0:
        error('No modules are available!')
        return []

    important('Modules available for download:', EMPHASIS)
    info('')
    if category is not None:
        info(f'  (Only showing modules in the [{category}] category)')

    for i, plugin in enumerate(plugins, 1):
        printPlugin(plugin, i)
    info('')

    if len(plugins) > 1:
        plugins = _selectMultiplePlugins(plugins)
    elif len(plugins) == 1:
        plugins = _selectOnePlugin(plugins[0])

    if len(plugins) == 0:
        important('No modules selected for installation!', EMPHASIS)
        return []

    important('Modules selected for installation:', EMPHASIS)
    for p in plugins:
        info(f'  {p.name}', EMPHASIS)

    return plugins


def _checkExist(dirname : Union[str, pathlib.Path]):
    """Sub-function of :func:`confirmDestination`. Checks to see if ``dirname``
    exists, and is a directory, and prints a warning if it doesn't.

    :arg dirname: Directory to check
    :returns:     ``True`` if ``dirname`` exists and is a directory, ``False``
                  otherwise.
    """

    if not (op.exists(dirname) and op.isdir(dirname)):
        error(f'{dirname} does not exist or is not a directory!')
        return False
    return True


def _perPluginDestination(plugins : List[Plugin]):
    """Sub-function of :func:`confirmDestination`. Prompts the user to
    specify an installation directory for every plugin.

    :arg plugins: List of :class:`.Plugin` objects representing the plugins
                  to be installed.
    """

    question('Select the installation directory '
             'for each module.', EMPHASIS)
    info('Press enter to accept the default installation '
         'directory for a module.', indent=2)

    for plugin in plugins:

        # loop until a valid response is provided
        while True:
            response = prompt(f'{plugin.name} ({plugin.destination}):')

            # empty response -> user accepts
            # default install destination
            if response == '': response = plugin.destination
            else:              response = expand(response)

            if _checkExist(response):
                plugin.destination = response
                break


def confirmDestination(plugins : List[Plugin]):
    """Prompts the user to confirm or select the destination directory for
    all of the ``plugins`` to be installed.

    The :attr:`.Plugin.destination` attribute of each plugin is modified
    to refer to the destination(s) selected by the user.
    """

    assert len(plugins) > 0
    assert all([p.destination is not None for p in plugins])

    instr = 'type the path into which you would like all modules to be ' \
            'installed, or type "customise" or "c" to set the installation ' \
            'directory separately for each module.'

    question('Where would you like the modules to be installed?', EMPHASIS)
    info('If you press enter without typing anything, the modules will be '
         'installed to the default destinations listed below. Otherwise, '
         f'{instr}', indent=2, wrap=True)
    important('Default installation directories:', EMPHASIS)
    maxlen = max([len(p.name) for p in plugins])
    for plugin in plugins:
        pad = ' ' * (maxlen - len(plugin.name))
        info(f'{plugin.name}:{pad}', EMPHASIS, end=' ', indent=2)
        info(plugin.destination)

    # loop until a valid response is provided
    while True:

        response = prompt('Enter installation directory:')

        # user wants to accept the defaults
        if response == '':
            # re-prompt if any default
            # destination does not exist
            if not all([_checkExist(p.destination) for p in plugins]):
                info(f'Please {instr}', wrap=True)
                continue

        # user has specified a path, and
        # thus asked for all plugins to be
        # installed into the same location
        elif response.lower() not in ('c', 'customise', 'customize'):
            response = expand(response)

            # re-prompt if destination is bad
            if not _checkExist(response):
                continue

            # otherwise set the destination for all plugins
            for p in plugins:
                p.destination = response

        # user wants to customise - ask them to
        # specify the destination directory for
        # each plugin
        else:
            _perPluginDestination(plugins)
        break


def downloadPlugin(plugin : Plugin, destDir, **kwargs) -> Tuple[str, bool]:
    """Downloads the plugin file described by the specified ``plugin``,

    If the file is already present in ``destDir``, and matches the
    plugin checksum (if a checksum is present for the plugin)  the file is
    not re-downloaded.

    All keyword arguments are passed through to the :func:`.downloadFile`
    function.

    :arg plugin:  :class:`.Plugin` describing the plugin file to be
                  downloaded.
    :arg destDir: Directory into which the plugin file should be saved.

    :returns:     A tuple containing:
                    - The full path to the downloaded file.
                    - ``True`` if the download was skipped, ``False``
                      otherwise.
    """

    fname        = plugin.fileName
    destFile     = op.join(destDir, fname)
    skipDownload = False

    info(f'{plugin.name}...', EMPHASIS)

    # if file exists, but we don't
    # have a checksum to verify it,
    # we assume that it is ok
    if op.exists(destFile) and plugin.checksum is None:
        skipDownload = True

    # otherwise we compare checksums,
    # and re-download the file if they
    # don't match
    elif op.exists(destFile) and plugin.checksum is not None:
        info(f'{fname} already exists - calculating SHA256 checksum...')
        destchecksum = calcChecksum(destFile)
        if destchecksum == plugin.checksum:
            info('Checksums match - skipping download')
            skipDownload = True
        else:
            info('Checksums do not match - re-downloading file')
            info(f'  manifest checksum:      {plugin.checksum}')
            info(f'  existing file checksum: {destchecksum}')
            os.remove(destFile)

    if not skipDownload:
        info(f'Downloading {plugin.url}')
        info(f'saving to {destFile}')
        downloadFile(plugin.url, destFile, **kwargs)

    return destFile, skipDownload


def verifyDownload(plugin : Plugin) -> bool:
    """Verifies the downloaded ``archiveFile`` against its checksum.

    :arg plugin: :class:`.Plugin` object describing the plugin
    :return:     ``True`` if the checksums match, ``False`` otherwise.
    """

    assert (plugin.archiveFile is not None) and op.exists(plugin.archiveFile)

    info(f'Downloaded archive file for {plugin.name} - calculating '
         'SHA256 checksum to verify download...')

    checksum              = calcChecksum(plugin.archiveFile)
    plugin.checksumPassed = checksum == plugin.checksum

    if plugin.checksumPassed:
        info('Checksums match - download succeeded')
    else:
        error(f'Downloaded archive file for {plugin.name} data set does not '
              'match checksum in manifest! This probably means that the '
              'download failed. I\'ll try to continue, but the file may be '
              'corrupt, so the next step might fail.', wrap=True)
        info(f'  manifest checksum:        {plugin.checksum}')
        info(f'  downloaded file checksum: {checksum}')

    return plugin.checksumPassed


def installPlugin(plugin : Plugin):
    """Attempts to install the given plugin. The :attr:`.Plugin.archiveFile`
    is extracted into the :attr:`.Plugin.desteination` directory.

    :arg plugin: :class:`.Plugin` object.
    """

    assert ((plugin.archiveFile is not None) and
            (plugin.destination is not None) and
            op.exists(plugin.archiveFile)    and
            op.exists(plugin.destination)    and
            op.isdir( plugin.destination))

    info(f'Installing [{plugin.name}] into {plugin.destination}', EMPHASIS)

    try:
        extractArchive(plugin.archiveFile, plugin.destination)

    except (CorruptArchive, Exception) as e:

        error(f'Could not extract [{plugin.name}] archive file: {e}')

        # delete the archive file if it is corrupt -
        # this indicates that the download did not
        # complete successfully
        if isinstance(e, CorruptArchive) and (plugin.checksumPassed is False):

            os.remove(plugin.archiveFile)
            plugin.archiveFile = None

            error(f'The {plugin.name} dataset may not have '
                  'been downloaded correctly - try re-running '
                  'this script to re-download it.', wrap=True)
