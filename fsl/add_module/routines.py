#!/usr/bin/env python
#
# routines.py - Miscellaneous routines for the fsl_add_module program.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module contains miscellaneous functions used by the ``fsl_add_module``
program.
"""


import os.path        as op
import                   os
import                   re
import                   math
import                   zlib
import                   shutil
import                   string
import                   hashlib
import                   pathlib
import                   urllib
import urllib.request as urlrequest

from typing import Union, List, BinaryIO

try:                import progressbar
except ImportError: progressbar = None

import fsl.add_module.admin as admin


def envvars(path: Union[str, pathlib.Path]) -> List[str]:
    """Identify all environment variables which are present in the given
    file path/string. Variables of the form ``$VARNAME`` and ``${VARNAME}``
    are identified.

    :returns: A list of tuples, with each tuple containing:
               - The full variable invocation, including the ``$`` and ``{}``
                 characters (e.g. ``'${VARNAME}'``)
               - The variable name (e.g. ``'VARNAME'``)
    """

    path     = str(path)
    matches  = []
    patterns = [
        r'(\$\{(\w+)\})',
        r'(\$(\w+))',
    ]

    for pat in patterns:
        matches.extend(re.findall(pat, path))

    return matches


def expandvars(path: Union[str, pathlib.Path]) -> str:
    """Alternative to ``os.path.expandvars`` which replaces unset variables
    with an empty string.
    """

    path = str(path)

    for fullmatch, name in envvars(path):
        path = path.replace(fullmatch, os.environ.get(name, ''))

    return path


def expand(path : Union[str, pathlib.Path]) -> str:
    """Applies ``os.path.abspath``, :func:`expandvars` and
    ``os.path.expanduser`` to ``path``.
    """
    return op.abspath(expandvars(op.expanduser(path)))


def sanitiseFileName(filename : str) -> str:
    """Generate a "sanitised" version of the given file name, with all silly
    characters replaced with underscores.
    """

    newname = ''
    allowed = string.ascii_letters + string.digits + '-_.'
    for char in filename:
        if char in allowed: newname += char
        else:               newname += '_'

    return newname


class DownloadFailed(Exception):
    """Exception type raised by the :func:`downloadFile` function if a
    download fails for some reason.
    """


class MockProgressBar:
    """Dummy progress bar used by the :func:`downloadFile` function when a
    real progress bar is not requested, or the ``progressbar2`` library is not
    installed.
    """
    def __init__(self, *args, **kwargs):
        pass
    def update(self, *args, **kwargs):
        pass
    def __enter__(self, *args, **kwargs):
        return self
    def __exit__(self, *args, **kwargs):
        pass


def _doDownload(req       : urlrequest.Request,
                outf      : BinaryIO,
                progress  : bool = True,
                blockSize : int  = 1048576):
    """Sub-function of :func:`downloadFile`.

    Takes data from ``req``, and streams it to ``outf``, ``blockSize``
    bytes at a time.

    :arg req:       ``urllib.request.Request`` to read data from.
    :arg outf:      File-like to save data to.
    :arg progress:  If ``True`` (the default), display a progress bar.
    :arg blockSize: Number of bytes to read/write in one iteration.
    """

    # disable progress bar if library is not installed
    progress = progress and (progressbar is not None)

    # Try and get the download size, so we
    # can display an accurate progress bar
    try:
        nbytes = int(req.headers['content-length'])
        nmb    = math.ceil(nbytes / 1048576)
    except KeyError:
        if progressbar is None: nmb = None
        else:                   nmb = progressbar.UnknownLength

    if progress: progress = progressbar.ProgressBar
    else:        progress = MockProgressBar

    mbcount = 0
    barargs = dict(max_value=nmb, format='%(value_s)s of %(max_value_s)sMB')

    # while we still have data to download:
    #  - read blockSize bytes
    #  - update progress bar
    #  - write out to file
    with progress(**barargs) as bar:
        bar.update(0)
        while True:
            block = req.read(blockSize)
            if len(block) == 0:
                break
            mbcount += len(block) / 1048576
            bar.update(int(mbcount))
            outf.write(block)


def downloadFile(url         : Union[str, pathlib.Path],
                 destination : Union[str, pathlib.Path],
                 progress    : bool = True,
                 resume      : bool = True,
                 blockSize   : int  = 1048576):
    """Download a file from ``url``, saving it to ``destination``.

    The file is initially saved to a temporary location, and then moved to
    ``destination`` once the download has completed. Therefore, if the
    download gets interrupted, it can be resumed.

    :arg url:         URL to download

    :arg destination: Path to save the file to

    :arg progress:    If ``True`` (the default), a progress bar is shown
                      while the file is being downloaded. Ignored if the
                      ``progressbar2`` library is not installed.

    :arg resume:      If ``True`` (the default), and an attempt to download
                      the file has been made previously, the download is
                      resumed.

    :arg blockSize:   Number of bytes to save to file within each iteration.
                      The download is performed in blocks, so a progress bar
                      can be displayed, and failed downloads can be resumed.
    """

    destination = op.abspath(destination)

    assert not op.exists(destination), \
           f'{destination} already exists!'
    assert     admin.canWrite(op.dirname(destination)), \
           f'Cannot write to {destination}!'

    # all files are saved to a temporary location
    # alongside the final destination. If a download
    # fails or is interrupted, the function can be
    # called again. We then check to see if this
    # temporary file exists, check its size if it
    # does, and attempt to resume the download from
    # that point.
    destdir  = op.dirname( destination)
    destname = op.basename(destination)
    tempdest = op.join(destdir, f'.{destname}.part')

    # start downloading from this offest
    if resume and op.exists(tempdest):
        offset     = op.getsize(tempdest)
        writeflags = 'ab'
        request    = urlrequest.Request(
            url, headers={'Range' : f'bytes={offset}-'})

    # or download the entire file
    else:
        writeflags = 'wb'
        request    = url

    try:
        with urlrequest.urlopen(request) as req:
            # if server does supports partial download via
            # the Range header, it will return HTTP 206.
            # Otherwise it will return 200, in which case
            # we have to re-download the entire file again.
            if req.getcode() == 200:
                writeflags = 'wb'

            with open(tempdest, writeflags) as outf:
                _doDownload(req, outf, progress, blockSize)

    except urllib.error.HTTPError as e:
        raise DownloadFailed(f'A network error has occurred while '
                             f'trying to download {destname}') from e

    # move to final destination when finished
    shutil.move(tempdest, destination)


def calcChecksum(path      : Union[str, pathlib.Path],
                 blockSize : int = 134217728) -> str:
    """Calculates a SHA256 checksum for the specified ``path``.

    :arg path:      Path of file to calculate a checksum for
    :arg blockSize: Number of bytes to read in at a time
    :returns:       String containing a SHA256 checksum
    """

    assert op.exists(path), f'{path} does not exist!'
    assert op.isfile(path), f'{path} is not a file!'
    assert blockSize > 0,   f'blockSize must be >0 ({blockSize})!'

    hashobj = hashlib.sha256()

    with open(path, 'rb') as f:
        block = f.read(blockSize)
        while block != b'':
            hashobj.update(block)
            block = f.read(blockSize)

    return hashobj.hexdigest()


class CorruptArchive(Exception):
    """Exception raised by the :func:`extractArchive` function if it appears
    that the archive file is corrupt.
    """


def extractArchive(archiveFile : Union[str, pathlib.Path],
                   destination : Union[str, pathlib.Path]):
    """Extracts the given ``archiveFile`` to the specified ``destination``.

    This is a wrapper around the :func:`_extractArchive` function - it is
    called to perform the actual extraction. If it appears that administrative
    privileges are required to write to the ``destination``, the call is
    performed via :func:`.admin.runAsAdmin`.

    A :exc:`CorruptArchive` exception is raised if it appears that the archive
    file is corrupted.

    If the call is performed via :func:`.admin.runAsAdmin`, and the call fails,
    a :mod:`RuntimeError` is raised.

    :arg archiveFile: File to be extracted
    :arg destination: Destination directory
    """

    if admin.canWrite(destination):
        _extractArchive(archiveFile, destination)
    else:
        admin.runAsAdmin('_extractArchive', archiveFile, destination)


def _extractArchive(archiveFile : Union[str, pathlib.Path],
                    destination : Union[str, pathlib.Path]):
    """Extracts the given ``archiveFile`` to the specified ``destination``.

    Any archive file format supported by ``shutil.unpack_archive`` is
    supported.
    """

    assert op.exists(     archiveFile), f'{archiveFile} does not exist!'
    assert op.isfile(     archiveFile), f'{archiveFile} is not a file!'
    assert op.exists(     destination), f'{destination} does not exist!'
    assert op.isdir(      destination), f'{destination} is not a directory!'
    assert admin.canWrite(destination), f'Cannot write to {destination}!'

    try:
        shutil.unpack_archive(archiveFile, destination)

    # If we get a decompress error, the
    # archive is probably corrupt
    except (zlib.error, EOFError, shutil.ReadError) as e:
        fname = op.basename(archiveFile)
        raise CorruptArchive('An error occurred while extracting '
                             f'the archive file {fname}') from e
