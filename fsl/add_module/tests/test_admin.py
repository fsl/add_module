#!/usr/bin/env python

import os
import os.path as op
import textwrap as tw
from unittest import mock

import pytest

import fsl.add_module.admin    as admin
import fsl.add_module.routines as routines

from . import tempdir, server, make_archive


def isroot():
    return os.geteuid() == 0

# this test can only work
# if run as a non-root user
@pytest.mark.noroottest
@pytest.mark.skipif('isroot()')
def test_canWrite():
    with tempdir():
        os.mkdir('testdir')
        assert admin.canWrite('testdir')
        os.chmod('testdir', 0o000)
        assert not admin.canWrite('testdir')
        os.chmod('testdir', 0o755)


@pytest.mark.skipif('os.name != "posix"')
def test_runAsAdmin():

    rootdir  = op.abspath(op.join(op.dirname(__file__), '..', '..', '..'))
    setupcfg = op.join(rootdir, 'setup.cfg')

    # a pass-through "sudo"
    dummy_sudo = tw.dedent("""
    #!/usr/bin/env bash
    "$@"
    """).strip()

    # sitecustomize.py, to make code
    # coverage work in subprocess calls
    # https://coverage.readthedocs.io/en/coverage-5.3/subprocess.html
    # https://docs.python.org/3/library/site.html
    sitecustomize = tw.dedent("""
    import coverage
    cov = coverage.process_startup()
    """).strip()

    with tempdir() as tdir, server() as srv:

        with open('sudo', 'wt') as f:
            f.write(dummy_sudo)
        os.chmod('sudo', 0o755)

        with open('sitecustomize.py', 'wt') as f:
            f.write(sitecustomize)

        make_archive('abc.zip', 'a/b', 'c/d')

        dldir   = 'download'
        destdir = 'dest'
        url     = f'http://localhost:{srv.port}/abc.zip'

        os.mkdir(dldir)
        os.mkdir(destdir)

        path    = os.environ['PATH']
        pypath  = os.environ.get('PYTHONPATH', '')
        path    = op.pathsep.join((tdir,    path))
        pypath  = op.pathsep.join((rootdir, pypath))
        pypath  = op.pathsep.join((tdir,    pypath))

        envpatch = {
            'PATH'                   : path,
            'PYTHONPATH'             : pypath,
            'COVERAGE_PROCESS_START' : setupcfg
        }

        with mock.patch.dict(os.environ, envpatch):
            admin.runAsAdmin('downloadFile',
                             url,
                             op.join(dldir, 'abc.zip'))
            admin.runAsAdmin('extractArchive', 'abc.zip', destdir)

        assert op.exists(op.join(dldir, 'abc.zip'))
        assert op.exists(op.join(destdir, 'a', 'b'))
        assert op.exists(op.join(destdir, 'c', 'd'))


def test_lookup():
    assert admin.lookup('downloadFile') is routines.downloadFile


def test_main():
    with tempdir():
        make_archive('abc.zip', 'a/b', 'c/d')
        admin.main(('extractArchive', 'abc.zip', '.'))

        assert op.exists(op.join('a', 'b'))
        assert op.exists(op.join('c', 'd'))
