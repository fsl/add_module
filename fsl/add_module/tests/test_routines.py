#!/usr/bin/env python
#
# test_routines.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import os.path as op
import shutil
import urllib.parse as urlparse
import urllib.request as urlrequest
from unittest import mock

import pytest

from . import tempdir, server

import fsl.add_module.routines as routines


def test_expandvars():

    tests = [
        ('$VAR',                 'VALUE'),
        ('$VAR/abcde/',          'VALUE/abcde/'),
        ('abcde/$VAR/abcde/',    'abcde/VALUE/abcde/'),
        ('$VAR$VAR',             'VALUEVALUE'),
        ('$VAR/abcde/$VAR',      'VALUE/abcde/VALUE'),
        ('${VAR}',               'VALUE'),
        ('${VAR}/abcde/',        'VALUE/abcde/'),
        ('abcde/${VAR}/abcde/',  'abcde/VALUE/abcde/'),
        ('${VAR}${VAR}',         'VALUEVALUE'),
        ('${VAR}/abcde/${VAR}',  'VALUE/abcde/VALUE'),
    ]

    with mock.patch.dict(os.environ, {'VAR' : 'VALUE'}):
        for input, expected in tests:
            assert routines.expandvars(input) == expected

    tests = [
        ('$VAR',          ''),
        ('${VAR}',        ''),
        ('$VAR/abcde/',   '/abcde/'),
        ('${VAR}/abcde/', '/abcde/'),
    ]
    with mock.patch.dict(os.environ, clear=True):
        for input, expected in tests:
            assert routines.expandvars(input) == expected


def test_expand():
    assert routines.expand('~')     == os.environ['HOME']
    assert routines.expand('$HOME') == os.environ['HOME']
    assert routines.expand('.')     == os.getcwd()

    with mock.patch.dict(os.environ, clear=True):
        assert routines.expand('$UNSET/dir') == '/dir'


def test_downloadFile():
    with tempdir():
        with open('file', 'wt') as f:
            f.write('1234567890')

        # download local file
        url = urlparse.urljoin('file:', urlrequest.pathname2url(
            op.abspath('file')))
        routines.downloadFile(url, 'downloaded')
        assert routines.calcChecksum('file') == \
               routines.calcChecksum('downloaded')

        os.remove('downloaded')

        # download over http
        with server() as srv:
            url = f'http://localhost:{srv.port}/file'
            routines.downloadFile(url, 'downloaded')
            assert routines.calcChecksum('file') == \
                   routines.calcChecksum('downloaded')

        # resume interrupted download
        os.remove('downloaded')
        with open('file2', 'wt') as f:
            f.write('1234567890')
            f.write('1234567890')

        # n.b. we can't actually test partial download,
        # because the http.server module (used by our
        # server() function) doesn't support the HTTP
        # Range header. But this at least tests the
        # fallback logic.
        shutil.copy('file', '.downloaded.part')
        with server() as srv:
            url = f'http://localhost:{srv.port}/file2'
            routines.downloadFile(url, 'downloaded')
            assert routines.calcChecksum('file2') == \
                   routines.calcChecksum('downloaded')
            assert not op.exists('.downloaded.part')


def test_calcChecksum():
    with tempdir():
        with open('data', 'wb') as f:
            f.write(b'1234567890')
        exp = 'c775e7b757ede630cd0aa1113bd102661ab38829ca52a6422ab782862f268646'
        got = routines.calcChecksum('data')
        assert got == exp

        with pytest.raises(AssertionError):
            routines.calcChecksum('data', 0)

    with pytest.raises(AssertionError):
        routines.calcChecksum('noexist')
    with pytest.raises(AssertionError):
        routines.calcChecksum(os.getcwd())


def test_extractArchive():
    with tempdir():
        os.mkdir('data')
        with open(op.join('data', 'rawdata'), 'wb') as f:
            f.write(b'1234567890')
        shutil.make_archive('archive', 'tar', root_dir='.', base_dir='data')

        os.mkdir('dest')
        routines.extractArchive('archive.tar', 'dest')
        assert routines.calcChecksum(op.join('data', 'rawdata')) == \
               routines.calcChecksum(op.join('dest', 'data', 'rawdata'))

        with mock.patch('fsl.add_module.admin.canWrite',   return_value=False), \
             mock.patch('fsl.add_module.admin.runAsAdmin', return_value=None):
            routines.extractArchive('archive.tar', 'dest')


def test__extractArchive():
    with tempdir():

        os.mkdir('data')
        with open(op.join('data', 'rawdata'), 'wb') as f:
            f.write(b'1234567890')
        shutil.make_archive('archive', 'tar', root_dir='.', base_dir='data')

        os.mkdir('dest')
        routines._extractArchive('archive.tar', 'dest')
        assert routines.calcChecksum(op.join('data', 'rawdata')) == \
               routines.calcChecksum(op.join('dest', 'data', 'rawdata'))

        with pytest.raises(AssertionError):
            routines._extractArchive('noexist', '.')
        with pytest.raises(AssertionError):
            routines._extractArchive('.', '.')
        with pytest.raises(AssertionError):
            routines._extractArchive('archive.tar', 'data/rawdata')

        with mock.patch('fsl.add_module.admin.canWrite', return_value=False):
            with pytest.raises(AssertionError):
                routines._extractArchive('archive.tar', '.')

        with pytest.raises(routines.CorruptArchive):
            routines._extractArchive(op.join('data', 'rawdata'), '.')
