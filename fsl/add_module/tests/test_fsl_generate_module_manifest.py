#!/usr/bin/env python
#
# test_fsl_generate_module_manifest.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os.path as op
import json
import shlex
import textwrap as tw

from . import tempdir, touch

import fsl.add_module.routines as routines

import fsl.scripts.fsl_generate_module_manifest as fgmm


def test_fsl_generate_module_manifest():
    plugins = ['1.zip', '2.zip', '3.zip']
    existing = tw.dedent("""
    [{
        "name": "1.zip",
        "url": "http://a.com/1.zip",
        "category": "cat1",
        "description": "desc1",
        "checksum": "baba",
        "size": 3
    },
    {
        "name": "2.zip",
        "url": "http://a.com/2.zip",
        "category": "cat2",
        "description": "desc2",
        "checksum": "abcdf",
        "size": 4
    },
    {
        "name": "3.zip",
        "url": "http://a.com/3.zip",
        "category": "cat3",
        "description": "desc3",
        "checksum": "aew",
        "size": 10
    }]
    """).strip()
    with tempdir():
        for p in plugins:
            touch(p)

        fgmm.main(shlex.split('-o manifest.json') + plugins)

        with open('manifest.json', 'rt', encoding='utf-8') as f:
            got = json.load(f)
        assert len(got) == len(plugins)
        for expp, gotp in zip(plugins, got):
            assert gotp['name']     == expp
            assert gotp['size']     == op.getsize(expp)
            assert gotp['checksum'] == routines.calcChecksum(expp)

        with open('existing.json', 'wt', encoding='utf-8') as f:
            f.write(existing)

        fgmm.main(shlex.split('-o manifest.json -m existing.json') + plugins)

        with open('manifest.json', 'rt', encoding='utf-8') as f:
            got = json.load(f)

        assert len(got) == len(plugins)
        for i, (expp, gotp) in enumerate(zip(plugins, got), 1):
            assert gotp['name']        == expp
            assert gotp['size']        == op.getsize(expp)
            assert gotp['checksum']    == routines.calcChecksum(expp)
            assert gotp['category']    == f'cat{i}'
            assert gotp['description'] == f'desc{i}'
            assert gotp['url']         == f'http://a.com/{i}.zip'
