#!/usr/bin/env python


import os
import os.path as op
import shutil
import tempfile
import threading
import functools as ft
import contextlib as ctxlib
import http.server as http
from unittest import mock


def touch(path):
    """Create the given ``path``."""
    with open(path, 'wt') as f:
        f.write(path)


@ctxlib.contextmanager
def tempdir():
    """Context manager to create, and change into, a temporary directory, and
    then afterwards delete it and change back to the original working
    directory.
    """
    with tempfile.TemporaryDirectory() as td:
        prevdir = os.getcwd()
        os.chdir(td)
        try:
            yield td
        finally:
            os.chdir(prevdir)


class HTTPServer(threading.Thread):
    """Simple HTTP server which serves files from a specified directory.

    Intended to be used via the :func:`server` context manager function.
    """
    def __init__(self, rootdir):
        threading.Thread.__init__(self)
        self.daemon = True
        self.rootdir = rootdir
        handler = ft.partial(http.SimpleHTTPRequestHandler, directory=rootdir)
        self.server = http.HTTPServer(('', 0), handler)

    def stop(self):
        self.server.shutdown()

    @property
    def port(self):
        return self.server.server_port

    def run(self):
        self.server.serve_forever()


@ctxlib.contextmanager
def server(rootdir=None):
    """Start a :class:`HTTPServer` on a separate thread to serve files from
    ``rootdir`` (defaults to the current working directory), then shut it down
    afterwards.
    """
    if rootdir is None:
        rootdir = os.getcwd()
    srv = HTTPServer(rootdir)
    srv.start()
    try:
        yield srv
    finally:
        srv.stop()


@ctxlib.contextmanager
def mock_input(*responses):
    """Mock the built-in ``input`` function so that it returns the specified
    sequence of ``responses``.

    Each response is returned from the ``input`` function in order, unless it
    is a callable, in which case it is called, and then the next non-callable
    response returned. This gives us a hacky way to manipulate things while
    stuck in an input REPL loop.
    """

    resp = iter(responses)

    def _input(*a, **kwa):
        n = next(resp)
        while callable(n):
            n()
            n = next(resp)
        return n

    with mock.patch('builtins.input', _input):
        yield


def make_archive(fname, *contents):
    """Create a compressed archive containing ``contents``, saving it to
    ``fname``. The archive type (``zip``, ``tar.gz``, etc) is determined from
    the file name.
    """

    fmts = {
        '.zip'     : 'zip',
        '.tar.gz'  : 'gztar',
        '.tar.bz2' : 'bztar',
        '.tar'     : 'tar'
    }

    fname    = op.abspath(fname)
    dirname  = op.dirname(fname)
    basename = None
    fmt      = None

    for f in fmts.keys():
        if fname.endswith(f):
            fmt      = fmts[f]
            basename = fname[:-len(f)]
            break
    else:
        raise ValueError(f'Unsupported archive format: {fname}')

    with tempdir():
        for c in contents:
            c = op.abspath(c)
            os.makedirs(op.dirname(c), exist_ok=True)
            touch(c)

        archive = op.join(dirname, basename)
        archive = shutil.make_archive(archive, fmt)
        shutil.move(archive, fname)


def check_dir(dirname, should_exist=None, should_not_exist=None):
    if should_exist     is None: should_exist     = []
    if should_not_exist is None: should_not_exist = []

    for f in should_exist:     assert     op.exists(op.join(dirname, f))
    for f in should_not_exist: assert not op.exists(op.join(dirname, f))
