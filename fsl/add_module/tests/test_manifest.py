#!/usr/bin/env python
#
# test_manifest.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import os.path as op
import json
import urllib.parse as urlparse
import urllib.request as urlrequest
from unittest import mock

from . import tempdir, touch

import fsl.add_module.manifest as plgman


def test_downloadManifest():
    testman = [
        {
            'url'  : 'http://abc.com/123',
        },
        {
            'url'  : 'http://abc.com/456',
            'name' : 'plugin456'
        },
        {
            'url'         : 'http://abc.com/789',
            'name'        : 'plugin789',
            'category'    : 'plgcat',
            'checksum'    : '12345',
            'description' : 'Great plugin',
            'destination' : '/dest',
        },
    ]
    with tempdir():

        with open('manifest.txt', 'wt') as f:
            json.dump(testman, f)

        url = urlparse.urljoin('file:', urlrequest.pathname2url(
            op.abspath('manifest.txt')))

        # local file or url should work
        manifest  = plgman.downloadManifest(url)
        lmanifest = plgman.downloadManifest('manifest.txt')

        assert manifest == lmanifest
        assert sorted(manifest.keys()) == ['123', 'plugin456', 'plugin789']
        assert manifest.categories == sorted(['plgcat', plgman.UNCATEGORISED])
        assert manifest['123']      .name        == '123'
        assert manifest['123']      .url         == 'http://abc.com/123'
        assert manifest['123']      .fileName    == '123'
        assert manifest['plugin456'].name        == 'plugin456'
        assert manifest['plugin456'].url         == 'http://abc.com/456'
        assert manifest['plugin456'].fileName    == '456'
        assert manifest['plugin789'].name        == 'plugin789'
        assert manifest['plugin789'].url         == 'http://abc.com/789'
        assert manifest['plugin789'].category    == 'plgcat'
        assert manifest['plugin789'].checksum    == '12345'
        assert manifest['plugin789'].description == 'Great plugin'
        assert manifest['plugin789'].destination == '/dest'
        assert manifest['plugin789'].fileName    == 'plgcat_789'


def test_addPlugin():
    with tempdir():
        manifest = plgman.Manifest()

        # url or local file should work
        touch('plugin1.zip')
        p1path = op.abspath('plugin1.zip')
        p1url = urlparse.urljoin('file:', urlrequest.pathname2url(p1path))
        manifest.addPlugin('plugin1.zip')
        manifest.addPlugin('http://abc.com/plugin2.zip')
        manifest.addPlugin('http://abc.com/plugin3.zip', name='plg3')
        assert sorted(manifest.keys()) == ['plg3', 'plugin1.zip', 'plugin2.zip']
        assert manifest['plugin1.zip'].url == p1url
        assert manifest['plugin2.zip'].url == 'http://abc.com/plugin2.zip'
        assert manifest['plg3']       .url == 'http://abc.com/plugin3.zip'

        # default/explicit category
        manifest.addPlugin('http://abc.com/plugin4')
        manifest.addPlugin('http://abc.com/plugin5', category='plgcat')
        manifest.addPlugin('http://abc.com/plugin6', name='plg6', category='plgcat')
        assert manifest['plugin4'].category == plgman.UNCATEGORISED
        assert manifest['plugin5'].category == 'plgcat'
        assert manifest['plg6']   .category == 'plgcat'

        # explicit destination, with tilde/var expansion
        manifest.addPlugin('http://abc.com/plugin7', destination='/usr/bin')
        manifest.addPlugin('http://abc.com/plugin8', destination='~')
        manifest.addPlugin('http://abc.com/plugin9', destination='a/b/c')
        with mock.patch.dict(os.environ, {'GROT' : '/usr/local/grot/'}):
            manifest.addPlugin('http://abc.com/plugin10', destination='$GROT')

        # defaults to FSLDIR if set
        with mock.patch.dict(os.environ, {'FSLDIR' : '/usr/local/fsl/'}):
            manifest.addPlugin('http://abc.com/plugin11')
        # or cwd if FSLDIR is not set
        with mock.patch.dict(os.environ, {}, clear=True):
            manifest.addPlugin('http://abc.com/plugin12')

        assert manifest['plugin7'] .destination == '/usr/bin'
        assert manifest['plugin8'] .destination == op.expanduser('~')
        assert manifest['plugin9'] .destination == op.join(os.getcwd(), 'a/b/c')
        assert manifest['plugin10'].destination == '/usr/local/grot'
        assert manifest['plugin11'].destination == '/usr/local/fsl'
        assert manifest['plugin12'].destination == os.getcwd()

        # overwrite if given same name
        manifest.addPlugin('http://abc.com/plugin13')
        assert manifest['plugin13'].url == 'http://abc.com/plugin13'
        manifest.addPlugin('http://abc.com/plugin14', name='plugin13')
        assert manifest['plugin13'].url == 'http://abc.com/plugin14'


def test_genManifest():
    m = plgman.Manifest()
    m['plugin1'] = plgman.Plugin(url='http://a.b.c.1', name='plugin1', category='abc')
    m['plugin2'] = plgman.Plugin(url='http://a.b.c.2', name='plugin2', description='Second')

    exp = [{'name' : 'plugin1', 'url' : 'http://a.b.c.1', 'category' : 'abc'},
           {'name' : 'plugin2', 'url' : 'http://a.b.c.2', 'category' : 'uncategorised',
            'description' : 'Second'}]

    got = plgman.genManifest(m)

    assert exp == json.loads(got)
