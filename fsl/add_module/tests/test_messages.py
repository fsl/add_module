#!/usr/bin/env python
#
# test_messages.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

from unittest import mock

import fsl.add_module.messages as msgs

from . import mock_input

def test_printmsg():

    msgs.info('abc')
    msgs.question('abc')
    msgs.warning('abc')
    msgs.error('abc')


def test_prompt():
    with mock_input('input'):
        assert msgs.prompt('abc') == 'input'
