#!/usr/bin/env python

import os
import os.path as op
import time
import json
import shutil
from unittest import mock

import pytest

import fsl.add_module.routines as routines
import fsl.scripts.fsl_add_module as fam

from . import make_archive, tempdir, server, mock_input, check_dir


def test_parseArgs():

    # either one destination is specified, or
    # <nplugins> destinationsx are specified
    fam.parseArgs('-d dest'.split())
    fam.parseArgs('-d dest a.zip'.split())
    fam.parseArgs('-d dest a.zip b.zip c.zip'.split())
    fam.parseArgs('-d dest1 -d dest2 -d dest3 a.zip b.zip c.zip'.split())

    with pytest.raises(SystemExit):
        fam.parseArgs('-d dest1 -d dest2 -d dest3 a.zip b.zip'.split())
    with pytest.raises(SystemExit):
        fam.parseArgs('-d dest1 -d dest2 -d dest3'.split())

    # destination should be expanded
    args = fam.parseArgs('-d ~/dest'.split())
    assert args.destination[0] == op.join(op.expanduser('~'), 'dest')

    with mock.patch.dict(os.environ, {'GROT' : 'somedir'}):
        args = fam.parseArgs('-d $GROT/dest'.split())
        assert args.destination[0] == op.abspath(op.join('somedir', 'dest'))


def test_loadManifest_different_sources():

    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip' },
        { 'name' : 'def', 'url'  : 'http://abc.com/def.zip' },
    ]

    # test

    #  - manifest from a URL
    #  - manifest from a file
    #  - invalid manifest
    #  - default manifest
    with tempdir(), server() as srv:
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        url  = f'http://localhost:{srv.port}/manifest.json'
        path = './manifest.json'

        # manifest from URL
        m, p = fam.loadManifest(fam.parseArgs(['-m', url]))
        assert len(p) == 0
        assert list(m.keys()) == ['abc', 'def']

        # manifest from file
        m, p = fam.loadManifest(fam.parseArgs(['-m', path]))
        assert len(p) == 0
        assert list(m.keys()) == ['abc', 'def']

        # invalid manifest - if no plugins
        # specified, an error should be raised
        with pytest.raises(RuntimeError):
            fam.loadManifest(fam.parseArgs(['-m', 'nomanifest']))

        # Specify plugin URL
        m, p = fam.loadManifest(
            fam.parseArgs(['-m', 'nomanifest', 'http://abc.com/plugin.zip']))
        assert p == ['plugin.zip']
        assert list(m.keys()) == ['plugin.zip']

        # default manifest
        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST', url):
            m, p = fam.loadManifest(fam.parseArgs([]))
            assert len(p) == 0
            assert list(m.keys()) == ['abc', 'def']


def test_loadManifest_merge_plugins_from_command_line():
    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip' },
        { 'name' : 'def', 'url'  : 'http://abc.com/def.zip' },
    ]

    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        args = fam.parseArgs(['-m', 'manifest.json',
                              'abc',
                              'ghi.zip',
                              'http://plugins.com/jkl.zip'])

        m, p = fam.loadManifest(args)
        assert p == ['abc', 'ghi.zip', 'jkl.zip']
        assert sorted(m.keys()) == ['abc', 'def', 'ghi.zip', 'jkl.zip']


def test_loadManifest_destination_specified():

    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip' },
        { 'name'        : 'def',
          'url'         : 'http://abc.com/def.zip',
          'destination' : '~/defdest', },
    ]


    # clear env incase FSLDIR is set
    with tempdir(), mock.patch.dict(os.environ, clear=True):
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        # no dest - plugins get their default
        args = fam.parseArgs(['-m', 'manifest.json'])
        m    = fam.loadManifest(args)[0]
        assert m['abc'].destination == os.getcwd()
        assert m['def'].destination == op.join(op.expanduser('~'), 'defdest')

        # one dest - all plugins get set to it
        dest = 'onedest'
        args = fam.parseArgs(['-m', 'manifest.json', '-d', dest])
        m    = fam.loadManifest(args)[0]
        assert m['abc'].destination == op.abspath(dest)
        assert m['def'].destination == op.abspath(dest)

        # one dest and one plugin - only that plugin gets set to it
        dest = 'onedest'
        args = fam.parseArgs(['-m', 'manifest.json', '-d', dest, 'def'])
        m    = fam.loadManifest(args)[0]
        assert m['abc'].destination == os.getcwd()
        assert m['def'].destination == op.abspath(dest)

        # one dest for each plugin - only valid
        # when plugins are requested on command-line
        dest1 = 'dest1'
        dest2 = 'dest2'
        with pytest.raises(SystemExit):
            args  = fam.parseArgs(['-m', 'manifest.json',
                                   '-d', dest1, '-d', dest2])
        args  = fam.parseArgs(['-m', 'manifest.json',
                               '-d', dest1, '-d', dest2,

                               'abc', 'def'])
        m     = fam.loadManifest(args)[0]
        assert m['abc'].destination == op.abspath(dest1)
        assert m['def'].destination == op.abspath(dest2)


def test_selectPlugins_from_filepath_and_url():
    with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST', None):
        args    = fam.parseArgs('abc.zip def.zip -d dest'.split())
        m, p    = fam.loadManifest(args)
        plugins = fam.selectPlugins(args, m, p)
        assert plugins == [m['abc.zip'], m['def.zip']]

        args    = fam.parseArgs('abc.zip http://plugins.com/def.zip '
                                '-d dest'.split())
        m, p    = fam.loadManifest(args)
        plugins = fam.selectPlugins(args, m, p)
        assert plugins == [m['abc.zip'], m['def.zip']]


def test_selectPlugins_name_from_manifest():
    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip' },
        { 'name' : 'def', 'url'  : 'http://abc.com/def.zip' },
    ]
    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        args    = fam.parseArgs('-m manifest.json -d dest '
                                'abc def ghi.zip'.split())
        m, p    = fam.loadManifest(args)
        plugins = fam.selectPlugins(args, m, p)
        assert plugins == [m['abc'], m['def'], m['ghi.zip']]


def test_selectPlugins_no_destination_specified():
    with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST', None):

        args    = fam.parseArgs('abc.zip def.zip'.split())
        m, p    = fam.loadManifest(args)

        with mock_input(''):
            plugins = fam.selectPlugins(args, m, p)
            assert plugins == [m['abc.zip'], m['def.zip']]


def test_selectPlugins_prompt_user():
    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip' },
        { 'name' : 'def', 'url'  : 'http://abc.com/def.zip' },
    ]
    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        args    = fam.parseArgs('-m manifest.json -d dest'.split())
        m, p    = fam.loadManifest(args)

    with mock_input('all'):
        plugins = fam.selectPlugins(args, m, p)
        assert plugins == [m['abc'], m['def']]


def test_selectPlugins_category():
    manifest = [
        { 'name' : 'abc', 'url'  : 'http://abc.com/abc.zip', 'category' : 'a' },
        { 'name' : 'def', 'url'  : 'http://abc.com/def.zip', 'category' : 'a' },
        { 'name' : 'ghi', 'url'  : 'http://abc.com/ghi.zip', 'category' : 'b' },
        { 'name' : 'jkl', 'url'  : 'http://abc.com/jkl.zip', 'category' : 'b' },
    ]
    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        args = fam.parseArgs('-m manifest.json -d dest -c a'.split())
        m, p = fam.loadManifest(args)

        with mock_input('all'):
            plugins = fam.selectPlugins(args, m, p)
            assert plugins == m.getCategory('a')

        args = fam.parseArgs('-m manifest.json -d dest -c b'.split())
        m, p = fam.loadManifest(args)

        with mock_input('all'):
            plugins = fam.selectPlugins(args, m, p)
            assert plugins == m.getCategory('b')


def test_download():
    # download from URL with good checksum
    # download from URL with bad checksum
    # download from local file
    # failed download
    with tempdir() as cwd, server() as srv:
        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')
        make_archive('jkl.zip', 'i/j', 'k/l')
        archivedir = op.join(cwd, 'archives')
        abcshasum  = routines.calcChecksum('abc.zip')
        manifest   = [
            { 'name'    : 'abc',
              'url'      : f'http://localhost:{srv.port}/abc.zip',
              'checksum' : f'{abcshasum}' },
            { 'name'     : 'def',
              'url'      : f'http://localhost:{srv.port}/def.zip',
              'checksum' : 'badchecksum', },
            { 'name'     : 'ghi',
              'url'      : f'http://localhost:{srv.port}/no_exist.zip', },
            { 'name'     : 'jkl',
              'url'      : f'{cwd}/jkl.zip', },
            { 'name'     : 'mno',
              'url'      : f'{cwd}/no_exist.zip', },
        ]
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        os.mkdir(archivedir)

        with mock.patch('fsl.scripts.fsl_add_module.ARCHIVE_DIR', archivedir):
            args    = fam.parseArgs('-m manifest.json'.split())
            m       = fam.loadManifest(args)[0]
            plugins = list(m.values())

            fam.download(args, plugins)

        assert     op.exists(op.join(archivedir, 'abc.zip'))
        assert     op.exists(op.join(archivedir, 'def.zip'))
        assert not op.exists(op.join(archivedir, 'ghi.zip'))
        assert not op.exists(op.join(archivedir, 'jkl.zip'))
        assert not op.exists(op.join(archivedir, 'mno.zip'))

        assert m['abc'].archiveFile == op.join(archivedir, 'abc.zip')
        assert m['def'].archiveFile == op.join(archivedir, 'def.zip')
        assert m['ghi'].archiveFile is None
        assert m['jkl'].archiveFile == op.join(cwd, 'jkl.zip')
        assert m['mno'].archiveFile is None
        assert m['abc'].checksumPassed
        assert m['def'].checksumPassed is False
        assert m['ghi'].checksumPassed is None
        assert m['jkl'].checksumPassed is None
        assert m['mno'].checksumPassed is None


def test_download_archiveDir():
    # - default archive dir
    # - user-specified archive dir
    with tempdir() as cwd, server() as srv:

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        argv = [f'http://localhost:{srv.port}/abc.zip',
                f'http://localhost:{srv.port}/def.zip',
                '-d', 'dest']

        adir1 = op.join(cwd, 'archives1')
        adir2 = op.join(cwd, 'archives2')

        os.mkdir(adir1)
        os.mkdir(adir2)

        with mock.patch('fsl.scripts.fsl_add_module.ARCHIVE_DIR', adir1):
            args    = fam.parseArgs(argv)
            m, p    = fam.loadManifest(args)
            plugins = fam.selectPlugins(args, m, p)

            fam.download(args, plugins)
            assert op.exists(op.join(adir1, 'abc.zip'))
            assert op.exists(op.join(adir1, 'def.zip'))
            assert m['abc.zip'].archiveFile == op.join(adir1, 'abc.zip')
            assert m['def.zip'].archiveFile == op.join(adir1, 'def.zip')

        args    = fam.parseArgs(['-a', adir2] + argv)
        m, p    = fam.loadManifest(args)
        plugins = fam.selectPlugins(args, m, p)
        fam.download(args, plugins)
        assert op.exists(op.join(adir2, 'abc.zip'))
        assert op.exists(op.join(adir2, 'def.zip'))
        assert m['abc.zip'].archiveFile == op.join(adir2, 'abc.zip')
        assert m['def.zip'].archiveFile == op.join(adir2, 'def.zip')


def test_install():

    with tempdir() as cwd:

        manifest = [
            { 'name'        : 'abc',
              'url'         : f'{cwd}/abc.zip',
              'destination' : f'{cwd}/abcdest/'},
            { 'name'        : 'def',
              'url'         : f'{cwd}/def.zip',
              'destination' : f'{cwd}/defdest/'},
        ]

        os.mkdir('abcdest')
        os.mkdir('defdest')

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        args     = fam.parseArgs('-m manifest.json abc.zip def.zip'.split())
        manifest = fam.loadManifest(args)[0]
        plugins  = list(manifest.values())

        fam.install(args, plugins)

        assert op.exists(op.join('abcdest', 'a', 'b'))
        assert op.exists(op.join('abcdest', 'c', 'd'))
        assert op.exists(op.join('defdest', 'e', 'f'))
        assert op.exists(op.join('defdest', 'g', 'h'))


def test_main_noargs():

    # with no args, should download
    # the default manifest, and ask
    # the user what plugins they want
    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip'},
            {'name' : 'def', 'url' : f'http://localhost:{srv.port}/def.zip'},
        ]

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')
        archiveDir = op.join(cwd, 'archives')

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        # patch os.environ in case FSLDIR is set
        fammod = 'fsl.scripts.fsl_add_module'
        with mock.patch(f'{fammod}.DEFAULT_MANIFEST', f'{cwd}/manifest.json'), \
             mock.patch(f'{fammod}.DEFAULT_CATEGORY', None), \
             mock.patch(f'{fammod}.ARCHIVE_DIR',      f'{cwd}/archives'), \
             mock.patch.dict(os.environ, clear=True):

            # user will be asked what plugins they want,
            # and will then be asked where they want to
            # install them
            with mock_input('all', ''):
                fam.main([])

            check_dir(cwd,
                [f'{archiveDir}/def.zip', 'a/b', 'c/d', 'e/f', 'g/h'],
                [f'{archiveDir}/abc.zip'])


def test_main_list():
    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip'},
            {'name' : 'def', 'url' : f'http://localhost:{srv.port}/def.zip'},
        ]
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST',
                        f'{cwd}/manifest.json'):
            fam.main(['-l'])
            fam.main('-l -v'.split())

        fam.main('-l -m manifest.json'.split())
        fam.main('-l -v -m manifest.json'.split())


def test_main_manifest_archiveDir():

    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip'},
            {'name' : 'def', 'url' : f'http://localhost:{srv.port}/def.zip'},
        ]

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')
        archiveDir = op.join(cwd, 'archives')

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        # patch os.environ in case FSLDIR is set
        with mock.patch.dict(os.environ, clear=True):

            with mock_input('all', ''):
                fam.main('-m manifest.json -a archives'.split())

            check_dir(cwd,
                [f'{archiveDir}/def.zip', 'a/b', 'c/d', 'e/f', 'g/h'],
                [f'{archiveDir}/abc.zip'])


def test_main_manifest_force():

    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip'},
            {'name' : 'def', 'url' : f'http://localhost:{srv.port}/def.zip'},
        ]

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        with mock.patch.dict(os.environ, clear=True):

            # give specific modules to download
            fam.main('-m manifest.json -a archives -f def'.split())
            check_dir(cwd,
                ['archives/def.zip', 'e/f', 'g/h'],
                ['archives/abc.zip', 'a/b', 'c/d'])

            shutil.rmtree('archives')
            shutil.rmtree('e')
            shutil.rmtree('g')

            # no modules specified - download all.
            # note that abc.zip is from cwd, so
            # is not copied into archive dir
            fam.main('-m manifest.json -a archives -f'.split())
            check_dir(cwd, ['archives/def.zip', 'e/f', 'g/h', 'a/b', 'c/d'])


def test_main_category():

    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip', 'category' : 'a'},
            {'name' : 'def', 'url' : f'{cwd}/def.zip', 'category' : 'b'},
        ]

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        with mock.patch.dict(os.environ, clear=True):
            with mock_input('y', ''):
                fam.main('-m manifest.json -a archives -c a'.split())
            check_dir(cwd, ['a/b', 'c/d'], ['e/f', 'g/h'])


def test_main_plugin_paths():
    with tempdir() as cwd, server() as srv:

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        args = f'./abc.zip http://localhost:{srv.port}/def.zip'.split()

        fammod = 'fsl.scripts.fsl_add_module'
        with mock.patch(f'{fammod}.DEFAULT_MANIFEST', None), \
             mock.patch(f'{fammod}.ARCHIVE_DIR',      f'{cwd}/archives'), \
             mock.patch.dict(os.environ, clear=True):

            with mock_input(''):
                fam.main(args)
            check_dir(cwd,
                ['a/b', 'c/d', 'e/f', 'g/h', 'archives/def.zip'],
                ['archives/abc.zip'])

            # force install
            os.mkdir('dest')
            fam.main(args + '-d dest -f'.split())
            check_dir('dest', ['a/b', 'c/d', 'e/f', 'g/h'])


def test_main_plugins_and_manifest():
    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name' : 'abc', 'url' : f'{cwd}/abc.zip'},
            {'name' : 'def', 'url' : f'http://localhost:{srv.port}/def.zip'},
        ]

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')
        make_archive('ghi.zip', 'i/j', 'k/l')
        make_archive('jkl.zip', 'm/n', 'o/p')

        args = '-m manifest.json -a archives abc def ghi.zip '\
               f'http://localhost:{srv.port}/jkl.zip'.split()

        with mock.patch.dict(os.environ, clear=True):

            with mock_input(''):
                fam.main(args)
            check_dir(cwd,
                ['a/b', 'c/d', 'e/f', 'g/h',
                 'i/j', 'k/l', 'm/n', 'o/p',
                 'archives/def.zip', 'archives/jkl.zip'],
                ['archives/abc.zip', 'archives/ghi.zip'])

            # force install
            os.mkdir('dest')
            fam.main(args + '-d dest -f'.split())
            check_dir('dest',
                ['a/b', 'c/d', 'e/f', 'g/h',
                 'i/j', 'k/l', 'm/n', 'o/p'])


def test_main_skip_already_downloaded():

    with tempdir() as cwd, server() as srv:

        make_archive('abc.zip',     'a/b', 'c/d')
        make_archive('abc_bad.zip', 'e/f', 'g/h')

        url = {
            'abc'     : f'http://localhost:{srv.port}/abc.zip',
            'abc_bad' : f'http://localhost:{srv.port}/abc_bad.zip',
        }

        manifest = [
            {'name'     : 'abc',
             'url'      : url['abc'],
             'checksum' : routines.calcChecksum('abc.zip')
             },
            {'name'     : 'abc_bad',
             'url'      : url['abc_bad'],
             'checksum' : routines.calcChecksum('abc_bad.zip')
             },
        ]

        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        archiveDir  = op.join(cwd, 'archives')
        destDir     = op.join(cwd, 'dest')
        archivePath = {
            'abc'     : op.join(archiveDir, 'abc.zip'),
            'abc_bad' : op.join(archiveDir, 'abc_bad.zip')
        }

        os.mkdir(destDir)
        os.mkdir(archiveDir)

        # We test via modification time, so sleep
        # a bit to make sure they're different
        # (note incorrect file for abc_bad, which
        # should cause checksum to fail and file
        # to be re-downloaded)
        shutil.copy('abc.zip', archivePath['abc'])
        shutil.copy('abc.zip', archivePath['abc_bad'])
        mtime = {
            'abc'     : os.stat(archivePath['abc'])    .st_mtime_ns,
            'abc_bad' : os.stat(archivePath['abc_bad']).st_mtime_ns,
        }

        time.sleep(1.5)

        # direct download - if file exists, it is
        # assumed to be ok, and not re-downloaded.
        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST', None):
            fam.main(f'{url["abc"]} -a {archiveDir} -d {destDir} -f'.split())
        assert os.stat(archivePath['abc']).st_mtime_ns == mtime['abc']
        check_dir(destDir, ['a/b', 'c/d'])

        # download via manifest - if file exists, and
        # checksums match, it is not re-downloaded
        shutil.rmtree(destDir)
        os.mkdir(destDir)
        fam.main(f'abc -a {archiveDir} -d {destDir} '
                 '-m manifest.json -f'.split())
        assert os.stat(archivePath['abc']).st_mtime_ns == mtime['abc']
        check_dir(destDir, ['a/b', 'c/d'])

        # if file exists, but checksums do not
        # match, file should be re-downloaded
        shutil.rmtree(destDir)
        os.mkdir(destDir)
        fam.main(f'abc_bad -a {archiveDir} -d {destDir} '
                 '-m manifest.json -f'.split())
        assert os.stat(archivePath['abc_bad']).st_mtime_ns != mtime['abc_bad']
        check_dir(destDir, ['e/f', 'g/h'])


def test_main_customise_plugin_dir():
    with tempdir() as cwd:
        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')
        make_archive('ghi.zip', 'i/j', 'k/l')

        abcdest = 'dest1'
        defdest = 'dest2'
        ghidest = 'dest3'
        os.mkdir(abcdest)
        os.mkdir(defdest)
        os.mkdir(ghidest)

        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST', None), \
             mock_input('c', abcdest, defdest, ghidest):
            fam.main(f'abc.zip def.zip ghi.zip'.split())

        check_dir(abcdest, ['a/b', 'c/d'])
        check_dir(defdest, ['e/f', 'g/h'])
        check_dir(ghidest, ['i/j', 'k/l'])


def test_main_default_destinations():
    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name'        : 'abc',
             'url'         : f'http://localhost:{srv.port}/abc.zip'},
            {'name'        : 'def',
             'url'         : f'http://localhost:{srv.port}/def.zip',
             'destination' : f'{cwd}/defdest/'},
        ]
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        make_archive('abc.zip', 'a/b', 'c/d')
        make_archive('def.zip', 'e/f', 'g/h')

        # default == FSLDIR if it is set
        os.mkdir('defdest')
        os.mkdir('fsl')
        with mock.patch.dict(os.environ, {'FSLDIR' : f'{cwd}/fsl'}):
            fam.main('abc def -m manifest.json -f'.split())
        check_dir('fsl',     ['a/b', 'c/d'])
        check_dir('defdest', ['e/f', 'g/h'])

        # default == cwd if FSLDIR is not set
        shutil.rmtree('defdest')
        shutil.rmtree('fsl')
        os.mkdir('defdest')
        with mock.patch.dict(os.environ, clear=True):
            fam.main('abc def -m manifest.json -f'.split())
        check_dir('.',       ['a/b', 'c/d'])
        check_dir('defdest', ['e/f', 'g/h'])
        shutil.rmtree('defdest')
        shutil.rmtree('a')
        shutil.rmtree('c')


def test_main_abort_bad_destination():
    with tempdir() as cwd:
        manifest = [
            {'name'        : 'abc',
             'url'         : 'abc.zip',
             'destination' : '$UNSET/bin'}
        ]
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        with pytest.raises(SystemExit) as e:
            with mock.patch.dict(os.environ, clear=True):
                fam.main('-m manifest.json -f abc'.split())
        assert e.value.code != 0


# In normal usage (when downloading the default
# manifest), fsl_add_module will only display
# modules in the "fsl_course_data" category.
# This can be overridden by passing
#   --category <some-category> or
#   --category all
def test_default_categories():
    with tempdir() as cwd, server() as srv:
        manifest = [
            {'name'        : 'a',
             'category'    : 'fsl_course_data',
             'url'         : f'http://localhost:{srv.port}/a.zip'},
            {'name'        : 'b',
             'url'         : f'http://localhost:{srv.port}/b.zip'},
            {'name'        : 'c',
             'category'    : 'patches',
             'url'         : f'http://localhost:{srv.port}/c.zip'},
            {'name'        : 'd',
             'category'    : 'fsl_course_data',
             'url'         : f'http://localhost:{srv.port}/d.zip'},
        ]
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(manifest))

        make_archive('a.zip', 'a/a.txt')
        make_archive('b.zip', 'b/b.txt')
        make_archive('c.zip', 'c/c.txt')
        make_archive('d.zip', 'd/d.txt')

        # default -> only fsl_course_data downloaded
        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST',
                        f'{cwd}/manifest.json'):
            fam.main('-f -a archives -d .'.split())
        check_dir('.',
                  ['a/a.txt', 'd/d.txt',
                   'archives/fsl_course_data_a.zip',
                   'archives/fsl_course_data_d.zip'],
                  ['b/b.txt', 'c/c.txt',
                   'archives/b.zip', 'archives/patches_c.zip'])

        shutil.rmtree('archives')
        shutil.rmtree('a')
        shutil.rmtree('d')

        # --category all -> all modules downloaded
        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST',
                        f'{cwd}/manifest.json'):
            fam.main('-f -a archives -d . -c all'.split())
        check_dir('.',
                  ['a/a.txt', 'd/d.txt', 'b/b.txt', 'c/c.txt',
                   'archives/fsl_course_data_a.zip',
                   'archives/fsl_course_data_d.zip',
                   'archives/b.zip',
                   'archives/patches_c.zip'])

        shutil.rmtree('archives')
        shutil.rmtree('a')
        shutil.rmtree('b')
        shutil.rmtree('c')
        shutil.rmtree('d')

        # --category <something> -> <something> modules downloaded
        with mock.patch('fsl.scripts.fsl_add_module.DEFAULT_MANIFEST',
                        f'{cwd}/manifest.json'):
            fam.main('-f -a archives -d . -c patches'.split())
        check_dir('.',
                  ['c/c.txt', 'archives/patches_c.zip'],
                  ['a/a.txt', 'b/b.txt', 'd/d.txt',
                   'archives/fsl_course_data_a.zip',
                   'archives/b.zip',
                   'archives/fsl_course_data_d.zip'])
