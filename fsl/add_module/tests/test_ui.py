#!/usr/bin/env python
#
# test_ui.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import shutil
import os.path as op
import json
from unittest import mock
import pathlib as plib

import pytest

import fsl.add_module.routines as routines
import fsl.add_module.manifest as plgman
import fsl.add_module.ui       as ui

from . import tempdir, server, mock_input, make_archive, touch


def test_downloadPluginManifest():

    testman = [
        { 'url'  : 'http://abc.com/123' }
    ]

    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(testman))

        man = ui.downloadPluginManifest('manifest.json')
        assert list(man.keys()) == ['123']
        assert man['123'].url   == 'http://abc.com/123'

        with server() as srv:
            url = f'http://localhost:{srv.port}/manifest.json'
            man = ui.downloadPluginManifest(url)
            assert list(man.keys()) == ['123']
            assert man['123'].url   == 'http://abc.com/123'

    man = ui.downloadPluginManifest('noexist')
    assert list(man.keys()) == []


def test_listPlugins():
    testman = [
        { 'url'  : 'http://abc.com/123' },
        { 'url'  : 'http://abc.com/456', 'category':'cat1' },
        { 'url'  : 'http://abc.com/789', 'category':'cat2' },
        { 'url'  : 'http://abc.com/012', 'category':'cat3',
          'description': 'Great plugin'}
    ]

    with tempdir():
        with open('manifest.json', 'wt') as f:
            f.write(json.dumps(testman))
        manifest = ui.downloadPluginManifest('manifest.json')
        ui.listPlugins(manifest)
        ui.listPlugins(manifest, True)


def test_createArchiveDir():
    with tempdir():
        ui.createArchiveDir(op.join('sub', 'dir', 'name'))
        ui.createArchiveDir(plib.Path('.') / 'pathlib' / 'dir')

        assert op.exists(op.join('sub', 'dir', 'name'))
        assert op.exists(op.join('pathlib', 'dir'))


def test_selectPlugins():
    manifest = plgman.Manifest()
    manifest.addPlugin('http://abc.com/123')
    manifest.addPlugin('http://abc.com/456')
    manifest.addPlugin('http://abc.com/789',
                       name='999',
                       description='great plugin',
                       category='this category',
                       checksum='12345')

    plugins = list(manifest.values())

    with mock_input('all'):
        selected = ui.selectPlugins(manifest)
        assert selected == plugins

    with mock_input('1 2 3'):
        selected = ui.selectPlugins(manifest)
        assert selected == plugins

    with mock_input('1'):
        selected = ui.selectPlugins(manifest)
        assert selected == [plugins[0]]

    with mock_input('1 3'):
        selected = ui.selectPlugins(manifest)
        assert selected == [plugins[0], plugins[2]]

    with mock_input('2 3'):
        selected = ui.selectPlugins(manifest)
        assert selected == plugins[1:]

    # re-prompt on bad input until
    # a valid response is given
    bad_input = ['bad input', '', '  ', '-1 0', '0 1', '3 4', '1 2 5', 'all']
    with mock_input(*bad_input):
        selected = ui.selectPlugins(manifest)
        assert selected == plugins


def test_selectPlugins_oneAvailable():
    manifest = plgman.Manifest()
    manifest.addPlugin('http://abc.com/123')

    plugins = list(manifest.values())

    with mock_input(''):
        assert ui.selectPlugins(manifest) == plugins
    with mock_input('y'):
        assert ui.selectPlugins(manifest) == plugins
    with mock_input('n'):
        assert ui.selectPlugins(manifest) == []


def test_selectPlugins_category():
    manifest = plgman.Manifest()
    manifest.addPlugin('http://abc.com/123', category='a')
    manifest.addPlugin('http://abc.com/456', category='b')
    manifest.addPlugin('http://abc.com/789', category='b')
    manifest.addPlugin('http://abc.com/abc', category='a')
    manifest.addPlugin('http://abc.com/def', category='a')
    manifest.addPlugin('http://abc.com/ghi', category='b')

    aplugins = manifest.getCategory('a')
    bplugins = manifest.getCategory('b')

    with mock_input('all'):
        assert ui.selectPlugins(manifest, 'a') == aplugins
    with mock_input('all'):
        assert ui.selectPlugins(manifest, 'b') == bplugins
    with mock_input('1 3'):
        assert ui.selectPlugins(manifest, 'a') == [aplugins[0], aplugins[2]]
    with mock_input('1 3'):
        assert ui.selectPlugins(manifest, 'b') == [bplugins[0], bplugins[2]]


def test_confirmDestination():

    with tempdir() as tdir:

        os.mkdir('fsl')
        os.mkdir('dest')

        fsldir   = op.abspath('fsl')
        destdir  = op.abspath('dest')
        manifest = plgman.Manifest()

        # destination defaults to FSLDIR
        # if it is set, or cwd otherwise
        with mock.patch.dict(os.environ, {}, clear=True):
            manifest.addPlugin('http://abc.com/123')
        with mock.patch.dict(os.environ, {'FSLDIR' : fsldir}):
            manifest.addPlugin('http://abc.com/456')

        # some other arbitrary destination dir
        manifest.addPlugin('http://abc.com/789', destination=destdir)

        # used below to reset manifest
        def resetdirs(dest=None):
            if dest is not None: d1, d2, d3 = dest, dest,   dest
            else:                d1, d2, d3 = tdir, fsldir, destdir
            manifest['123'].destination = d1
            manifest['456'].destination = d2
            manifest['789'].destination = d3

        # empty response -> accept defaults
        with mock_input(''):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir

        # re-prompt if default dir for any plugin does not exist
        manifest['456'].destination = op.abspath('noexist')
        with mock_input('', resetdirs, ''):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir

        # install all plugins into the same destination
        with mock_input('dest'):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == destdir
        assert manifest['456'].destination == destdir
        assert manifest['789'].destination == destdir

        # but reprompt if that destination doesn't exist
        resetdirs()
        with mock_input('noexist', 'dest'):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == destdir
        assert manifest['456'].destination == destdir
        assert manifest['789'].destination == destdir

        # customise for each plugin
        with mock_input('c', tdir, fsldir, destdir):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir

        # customise for each plugin - empty response
        # for a plugin means keep its default
        resetdirs()
        with mock_input('c', '', '', ''):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir

        # re-prompt on any bad response
        resetdirs()
        with mock_input('c', 'noexist', '', '', ''):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir

        # re-prompt on any non-existent response
        resetdirs('noexist')
        with mock_input('c',
                        'noexist', tdir,
                        'noexist', fsldir,
                        'noexist', destdir):
            ui.confirmDestination(manifest.values())
        assert manifest['123'].destination == tdir
        assert manifest['456'].destination == fsldir
        assert manifest['789'].destination == destdir


def test_downloadPlugin():
    with tempdir(), server() as srv:

        destdir = 'destination'
        make_archive('abc.zip', 'a/b/c.txt', 'd/e/f.txt')
        make_archive('ghi.zip', 'g/h/i.txt', 'j/k/l.txt')
        os.mkdir(destdir)

        plugin1 = plgman.Plugin(
            name='abc.zip',
            url=f'http://localhost:{srv.port}/abc.zip',
            checksum=routines.calcChecksum('abc.zip'))

        plugin2 = plgman.Plugin(
            name='ghi.zip',
            url=f'http://localhost:{srv.port}/ghi.zip',
            category='ghicat',
            checksum=routines.calcChecksum('ghi.zip'))

        # regular download
        ret = ui.downloadPlugin(plugin1, destdir)
        assert op.exists(op.join(destdir, 'abc.zip'))
        assert routines.calcChecksum(op.join(destdir, 'abc.zip')) == \
               plugin1.checksum
        assert op.abspath(ret[0]) == op.abspath(op.join(destdir, 'abc.zip'))
        assert not ret[1]

        # regular download, checking
        # that downloaded file is
        # named <category>_<file>
        ret = ui.downloadPlugin(plugin2, destdir)
        assert op.exists(op.join(destdir, 'ghicat_ghi.zip'))
        assert routines.calcChecksum(op.join(destdir, 'ghicat_ghi.zip')) == \
               plugin2.checksum
        assert op.abspath(ret[0]) == op.abspath(op.join(destdir, 'ghicat_ghi.zip'))
        assert not ret[1]

        # file already exists and checksums
        # match - download should be skipped
        with mock.patch('fsl.add_module.ui.downloadFile') as mocked:
            ret = ui.downloadPlugin(plugin1, destdir)
            mocked.assert_not_called()
            assert op.abspath(ret[0]) == op.abspath(op.join(destdir, 'abc.zip'))
            assert ret[1]

        # file already exists but checksums
        # do not match - download should be
        # repeated
        with mock.patch('fsl.add_module.ui.downloadFile') as mocked, \
             mock.patch.object(plugin1, 'checksum', '1234'):
            ret = ui.downloadPlugin(plugin1, destdir)
            mocked.assert_called()
            assert op.abspath(ret[0]) == op.abspath(op.join(destdir, 'abc.zip'))
            assert not ret[1]


def test_verifyDownload():
    with tempdir():
        make_archive('abc.zip', 'a/b/c.txt', 'd/e/f.txt')

        # checksums match
        plugin = plgman.Plugin(
            name='abc.zip',
            url='http://localhost/abc.zip',
            checksum=routines.calcChecksum('abc.zip'),
            archiveFile=op.abspath('abc.zip'))
        assert ui.verifyDownload(plugin)
        assert plugin.checksumPassed

        # checksums do not match
        with mock.patch.object(plugin, 'checksum', '1234'):
            assert not ui.verifyDownload(plugin)
            assert not plugin.checksumPassed

        # archiveFile attribute not set
        with mock.patch.object(plugin, 'archiveFile', None), \
             pytest.raises(AssertionError):
            ui.verifyDownload(plugin)

        # archiveFile does not exist
        with mock.patch.object(plugin, 'archiveFile', 'noexist.zip'), \
             pytest.raises(AssertionError):
            ui.verifyDownload(plugin)


def test_installPlugin():
    with tempdir():

        destdir = op.abspath('destination')
        make_archive('abc.zip', 'a/b/c.txt', 'd/e/f.txt')

        os.mkdir(destdir)

        plugin = plgman.Plugin(name='abc.zip',
                               url='http://localhost/abc.zip',
                               archiveFile=op.abspath('abc.zip'),
                               destination=destdir)

        # errors if archive file or destination are unset or invalid
        with mock.patch.object(plugin, 'archiveFile', None), \
             pytest.raises(AssertionError):
            ui.installPlugin(plugin)
        with mock.patch.object(plugin, 'archiveFile', 'noexist.zip'), \
             pytest.raises(AssertionError):
            ui.installPlugin(plugin)
        with mock.patch.object(plugin, 'destination', None), \
             pytest.raises(AssertionError):
            ui.installPlugin(plugin)
        with mock.patch.object(plugin, 'destination', 'noexist'), \
             pytest.raises(AssertionError):
            ui.installPlugin(plugin)
        touch('dummy')
        with mock.patch.object(plugin, 'destination', 'dummy'), \
             pytest.raises(AssertionError):
            ui.installPlugin(plugin)

        # normal usage
        ui.installPlugin(plugin)
        assert op.exists(op.join(destdir, 'a', 'b', 'c.txt'))
        assert op.exists(op.join(destdir, 'd', 'e', 'f.txt'))

        shutil.rmtree(destdir)
        os.mkdir(destdir)

        # failures are absorbed (an error message is printed)
        with mock.patch('fsl.add_module.ui.extractArchive',
                        side_effect=Exception):
            ui.installPlugin(plugin)
            assert plugin.archiveFile == op.abspath('abc.zip')
            assert op.exists('abc.zip')
        with mock.patch('fsl.add_module.ui.extractArchive',
                        side_effect=routines.CorruptArchive):
            ui.installPlugin(plugin)
            assert plugin.archiveFile == op.abspath('abc.zip')
            assert op.exists('abc.zip')

        # but if checksum failed, and corruptarchive error
        # occurs, the archive file should be deleted
        with mock.patch('fsl.add_module.ui.extractArchive',
                        side_effect=routines.CorruptArchive), \
             mock.patch.object(plugin, 'checksumPassed', False):
            ui.installPlugin(plugin)
            assert plugin.archiveFile is None
            assert not op.exists('abc.zip')
