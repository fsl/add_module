# `fsl_add_module`

[![Pipeline status](https://git.fmrib.ox.ac.uk/fsl/add_module/badges/master/pipeline.svg)](https://git.fmrib.ox.ac.uk/fsl/add_module/-/commits/master)
[![Test coverage](https://git.fmrib.ox.ac.uk/fsl/add_module/badges/master/coverage.svg)](https://git.fmrib.ox.ac.uk/fsl/add_module/commits/master/)



`fsl_add_module` is a script which can be used to download FSL "module", or
"plugin" files. It is installed as a part of FSL 6.0.5 and newer.

`fsl_add_module` requires Python 3, and the third-party `progressbar2` library.

A FSL module is simply a compressed archive file (e.g. `.zip`, `.tar.gz`, etc)
which is intended to be installed into `$FSLDIR`, and which is accessible via a
HTTP URL. The `fsl_add_module` script will download and install module files
requested by the user.


## Usage

`fsl_add_module` can be executed like so:

    fsl_add_module


When called like this, the script will download a "manifest" file from a fixed
URL, which is a JSON file containing a list of all modules that are available,
and where they can be downloaded from. Once the manifest file has been
downloaded, `fsl_add_module` will display a list of all available modules, and
will prompt the user to select which modules they would like to download.


It is also possible to request which modules you would like to install:

    fsl_add_module module_a module_b module_c


When called like this, the script will download the manifest file, look up the
URLs of the requseted modules, and will then download and install them.


Finally, it is possible to specify module files directly by a URL, or a path
to a local file:

    fsl_add_module http://fsl.fmrib.ox.ac.uk/downloads/module.zip
    fsl_add_module ~/Downloads/module.zip


By default, all modules are installed into `$FSLDIR` or, if `$FSLDIR` is not
set, the current working directory. This may be overridden in the manifest file,
which may specify a particular destination directory for each plugin. Or, the
destination directory may be specified at the command-line, via the `-d` option:

    fsl_add_module module_a -d /path/to/destination/directory

In all cases, the user will be asked to confirm the destination directory for
each requested module, unless the `-f`/`--force` option is used, in which case
all requested modules will be downloaded and installed without confirmation:

    fsl_add_module module_a -f


## Examples

A set of example module files, and an example manifest file, are contained in
the ``tesdata/`` sub-directory. To use these examples, navigate into the
``testdata/`` directory, and start a HTTP server listening on port 8000, e.g.:

    cd testdata
    python3 -m http.server 8000

Now call the ``fsl_add_module`` script like so:

    fsl_add_module -m http://localhost:8000/manifest.json
