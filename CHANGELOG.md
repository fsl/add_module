# `fsl_add_module` changelog


## 0.5.1 (Thursday 3rd August 2023)

 - Adjusted the `fsl_generate_module_manifest` command so that it does not
   expand the `destination` path when reading from an existing manifest file.


## 0.5.0 (Monday 5th June 2023)

 - New `fsl_generate_module_manifest` command, to generate a JSON manifest
   for a set of files.
 - Added ability to refer to 'built-in' manifests by aliases - the default
   manifest is referred to as `fslcourse`, and the Graduate Programme manifest
   as `gradcourse`, e.g. `fsl_add_module -m gradcourse`.
 - Added support for a `size` field in manifest entries, specifying the file
   size in bytes.


## 0.4.0 (Tuesday 24th January 2023)

 - Changed the default behaviour so that only files in the `fsl_course_data`
   category are downloaded. This can be overridden by specifying `--category
   all`.
 - Prefix downloaded files with their category name to reduce the likelihood
   of collisions across different categories (e.g. `registration.tar.gz` from
   the FSL course and from grad course data sets). Nothing is done to prevent
   collisions in extracted paths from different archives though.
 - The `--force` option can now be used without specifying any modules - it
   causes all modules in the default/selected cateogories to be downloaded.


## 0.3.2 (Wednesday 1st September 2021)

 - Changed the default manifest URL to
   http://fsl.fmrib.ox.ac.uk/fslcourse/downloads/manifest.json, which
   describes the FSL course data sets.
 - Fixed a typo in a UI message.


## 0.3.1 (Tuesday 25th May 2021)

 - Fixed an issue with building the `fsl_add_module` package.


## 0.3.0 (Tuesday 25th May 2021)

 - Plugin manifest entries can now contain `version` and `terms_of_use` fields
   which, if present, are displayed during installation.
 - The `fsl` and `fsl.scripts` packages are now defined as native namespace
   packages, where they were previously defined as `pkgutil`-style namespace
   packages.


## 0.2.0 (Monday 2nd November 2020)

 - Initial release of the `fsl_add_module` script.


## 0.1.0 (Wednesday 30th September 2020)

 - Initial deployment in the form of the `fsldl` script, used for downloading
   FSL course data from https://fsl.fmrib.ox.ac.uk/fslcourse/#Data
