#!/usr/bin/env bash

set -e

pip install -e ".[test]"

# pytest can have trouble with
# native namespace packages
export PYTHONPATH=$(pwd)

# noroottest tests will fail/be skipped
# if not executed as a non-root user, so
# we run them as nobody
chmod -R a+w `pwd`
su -s /bin/bash -c "pytest -m noroottest --cov-report= --cov-append"  nobody
pytest -m "not noroottest" --cov-report= --cov-append

python -m coverage report -i
