#!/usr/bin/env bash
pip install wheel setuptools twine build
python -m build
twine check dist/*

# do a test install from both source and wheel
sdist=`find dist -maxdepth 1 -name *.tar.gz`
wheel=`find dist -maxdepth 1 -name *.whl`

for target in $sdist $wheel; do
    python -m venv test.venv
    . test.venv/bin/activate
    pip install --upgrade pip setuptools wheel
    pip install $target
    fsl_add_module -V
    fsl_add_module -h
    deactivate
    rm -r test.venv
done
